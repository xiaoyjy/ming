<?php


class CY_Model_Mongo
{
	protected $db;
	protected $table;

	function __construct($table)
	{
		$this->db    = new CY_Util_Mongo();
		$this->table = $table;
	}

	function mGet($kv, $options = [])
	{
		return $this->db->mGet($this->table, $kv, $options);
	}

	function mSet($data, $options = [])
	{
		return $this->db->mSet($this->table, $data, $options);
	}

	function delete($kv, $options = [])
	{
		return $this->db->delete($this->table, $kv, $options);
	}

	function update($kv, $data, $options = [])
	{
		return $this->db->update($this->table, $data, $kv, $options);
	}

}

?>
