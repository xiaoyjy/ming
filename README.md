
INSTALL
```
git clone git@bitbucket.org:xiaoyjy/ming.git
git clone git@bitbucket.org:xiaoyjy/cye.git

cd cye
phpize
./configure
make 
make install
```

Ming系统简介
=====================
由抓取（crawler）, 扩散(filter), 抽取(extractor), 投递(delivery) ，代理管理（proxy）五个模块构成

依赖的软件
=====================
* PHP以及PHP的扩展amqp、curl、msgpack、mysqli、mysqlnd、mcrypt、iconv、tidy
* Rabbitmq
* MySQL
* PHP以及相关扩展
* 装好PHP以后，需要安装一个自己写的扩展cye，源代码在git@bitbucket.org:xiaoyjy/cye.git下面，务必在cURL扩展安装完成之后安装
* supervise


部署rabbitmq
=====================
* 安装好之后，在etc/rabbitmq/rabbitmq-env.conf 中修改RABBITMQ_MNESIA_BASE 确保把它放到一个空间比较大的磁盘下了
```
例如：
# vim /etc/rabbitmq/rabbitmq-env.conf 

RABBITMQ_MNESIA_BASE=/data/rabbitmq
RABBITMQ_NODE_IP_ADDRESS=127.0.0.1
```

* 启动rabbitmq
```
/etc/init.d/rabbitmq-server start
(每个系统可能不一样，需跟据自身系统情况而定)
```

* 添加vhost
```
rabbitmqctl add_vhost ming
```

* 添加user
```
rabbitmqctl add_user ming ming
(用户名密码都是ming)
```

* 把vhost ming对用户’ming’授权
```
rabbitmqctl set_permissions -p ming ming '.*' '.*' '.*'
```

配置数据库与队列系统
=====================
* 打开`ming/etc/backends.php`
* 配置数据库示例
```
$_ENV['config']['db'] = array
(
 [
 'host' => '127.0.0.1',
 'port' => 3306,
 'user' => 'ming',
 'password' => 'mysqld',
 'database' => 'ming'
 ]
);
```

* 配置队列系统示例
```
$_ENV['config']['mq_ming'] = array
(
 [
 'host' => '127.0.0.1',
 'port' => 5672,
 'login' => 'ming',
 'vhost' => 'ming',
 'password' => 'ming'
 ]
);
```

* 配置代理池，这个省略300字...
=====================
```
$_ENV['config']['proxy_fetch_url'] = 'http://www.xinxinproxy.com/httpip/json?orderId=xxxxxxxxxxxxxx&count=1000';
$_ENV['config']['proxy_url'] = 'http://www.xinxinproxy.com/httpip/json?orderId=xxxxxxxxxxxxxx';
```

配置HTTP API访问接口示例
=====================
* Apache 配置方法
```
<VirtualHost *:80>

        ServerName ming.example.com
        DocumentRoot /ming path/www

        RewriteEngine On

        RewriteCond %{DOCUMENT_ROOT}%{REQUEST_FILENAME} !-f
        RewriteCond %{DOCUMENT_ROOT}%{REQUEST_FILENAME} !-d
        RewriteRule . /index.php [L]

        ErrorLog /var/log/apache2/ming-error.log
        CustomLog /var/log/apache2/ming-access.log combined

</VirtualHost>
```

创建supervise控制目录
=====================
```
mkdir run
cd run
mkdir crawlerd deliveryd extractord filterd proxyd

echo -e '#!/bin/bash\n\ncd ../../\nsbin/crawlerd\n' > crawlerd/run
chmod +x crawlerd/run

echo -e '#!/bin/bash\n\ncd ../../\nsbin/deliveryd\n' > deliveryd/run
chmod +x deliveryd/run

echo -e '#!/bin/bash\n\ncd ../../\nsbin/extractord\n' > extractord/run
chmod +x extractord/run

echo -e '#!/bin/bash\n\ncd ../../\nsbin/filterd\n' > filterd/run
chmod +x filterd/run

echo -e '#!/bin/bash\n\ncd ../../\nsbin/proxyd\n' > proxyd/run
chmod +x proxyd/run

```

更改目录权限，有两个目录需要与Apache保持相同的用户权限
=====================
```
chown -R apache:apache run log
```

启动一下Apache
=====================
```
/etc/init.d/apache start
```

启动各项进程
=====================
```
nohup sudo -u apache supervise run/proxyd &
nohup sudo -u apache supervise run/crawlerd &
nohup sudo -u apache supervise run/filterd &
nohup sudo -u apache supervise run/extractord &
```

好了，现在可以上管理系统上调度你的抓取任务了
