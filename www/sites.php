<?php

include dirname(__DIR__).'/src/init.php';

$title = isset($_GET['title']) ? $_GET['title'] : '';
$name  = isset($_GET['name'] ) ? $_GET['name' ] : '';
$host  = isset($_GET['host'] ) ? $_GET['host' ] : '';
$url   = isset($_GET['url']  ) ? $_GET['url'  ] : '';
$cmd   = isset($_GET['cmd']  ) ? $_GET['cmd'  ] : 'add';
$standalone = isset($_GET['standalone']) ? $_GET['standalone'] : 0;

$fm = new CY_Model_Default('filters');
$sm = new CY_Model_Default('sites');
$db = new CY_Util_MySQL();
switch($cmd)
{
	case 'add':
		if(empty($name) || empty($host))
		{
			exit(json_encode(['errno' => '10001', 'error' => "no name or host"]));
		}

		$sites['name' ] = $name;
		$sites['title'] = isset($title) ? $title : $host;
		$sites['host' ] = $host;
		$sites['url'  ] = empty($url) ? 'http://'.$host : $url;
		$sites['ctime'] = date('Y-m-d H:i:s');
		$sites['standalone'] = $standalone;
		$sm->mSet([$sites], ['update' => true]);

		$dt = $sm->mGet(['name' => $name]);
		if(empty($dt['data'][$name]['id']))
		{
			exit(json_encode(['errno' => '10002', 'error' => "Can not get site_id"]));
		}

		$parts = parse_url($url);

		$site_id = $dt['data'][$name]['id'];
		$filter['site_id'] = $site_id;
		//$filter['host'] = $host;
		$filter['rule'] = isset($rule) ? $rule : $parts['scheme'].'://'.$host.'/.*?\.html';
		$filter['enable'] = 1;
		$filter['weight'] = 5;
		$filter['ctime'] = date('Y-m-d H:i:s');
		$dt = $fm->mGet(['host' => $host]);
		if(empty($dt['data']))
		{
			$fm->mSet([$filter]) && $dt = $fm->mGet(['host' => $host]);;
		}

		if($standalone)
		{
			$tables = get_create_table_sql($name);
			foreach($tables as $sql)
			{
				$db->query($sql);
			}

			$table_pre = $name.'_';
		}
		else
		{
			$table_pre = '';
		}

		$md5 = md5($sites['url']);
		$sql = "INSERT INTO `".$table_pre."urls` set `site_id`='$site_id', `host`='$host', ".
				" `url`='{$sites['url']}', `md5`='".$md5.
				"', weight=9, ctime=now() ON DUPLICATE KEY UPDATE `url`=values(`url`)";
		$dt  = $db->query($sql);
		if(isset($dt['data']['insert_id']))
		{
			$id = $dt['data']['insert_id'];
			if(!$id)
			{
				$sql = 'SELECT id FROM `'.$table_pre.'urls` WHERE md5="'.$md5.'"';
				$dt1 = $db->query($sql);
				$id  = isset($dt1['data'][0]['id']) ? $dt1['data'][0]['id'] : 0;
			}

			$sql = "INSERT INTO `{$table_pre}urlstats` set `id`=$id, ctime=now()";
			$db->query($sql);

			$uq = new CY_Util_mQueue();
			$uq->init($table_pre.'urls');
			$uq->send($id);
			//$uq->send(json_encode(['id' => $id, 'name' => $name]));
		}

		if($standalone)
		{
			system('mkdir -p '.dirname(__DIR__).'/run/'.$name.'_crawlerd');
			system('mkdir -p '.dirname(__DIR__).'/run/'.$name.'_filterd');
			system('mkdir -p '.dirname(__DIR__).'/run/'.$name.'_extractord');
			system('mkdir -p '.dirname(__DIR__).'/run/'.$name.'_data');

			$cmd1 = "#!/bin/bash\n\ncd ../../\n\nsbin/crawlerd $name\n";
			$cmd2 = "#!/bin/bash\n\ncd ../../\n\nsbin/filterd $name\n";
			$cmd3 = "#!/bin/bash\n\ncd ../../\n\nsbin/extractord $name\n";
			$cmd4 = "#!/bin/bash\n\ncd ../../\n\nsbin/deliveryd $name\n";
			system("echo '$cmd1' > ".dirname(__DIR__).'/run/'.$name.'_crawlerd/run');
			system("echo '$cmd2' > ".dirname(__DIR__).'/run/'.$name.'_filterd/run');
			system("echo '$cmd3' > ".dirname(__DIR__).'/run/'.$name.'_extractord/run');
			system("echo '$cmd4' > ".dirname(__DIR__).'/run/'.$name.'_data/run');

			system("chmod +x ".dirname(__DIR__).'/run/'.$name.'_crawlerd/run');
			system("chmod +x ".dirname(__DIR__).'/run/'.$name.'_filterd/run');
			system("chmod +x ".dirname(__DIR__).'/run/'.$name.'_extractord/run');
			system("chmod +x ".dirname(__DIR__).'/run/'.$name.'_data/run');
		}


		echo json_encode(['errno' => 0, 'data' => ['site_id' => $site_id, 'name' => $name]]);
		break;

	default:
		break;

}

function get_create_table_sql($name)
{
	$array = [];
	$array['urls'] = <<<SQL
		CREATE TABLE `{$name}_urls` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`site_id` int(11) unsigned NOT NULL DEFAULT 0,
		`md5` varchar(32) NOT NULL DEFAULT '',
		`url` varchar(255) NOT NULL DEFAULT '',
		`host` varchar(64) NOT NULL DEFAULT '',
		`weight` int(11) unsigned NOT NULL DEFAULT '5',
		`timeout` int(11) unsigned NOT NULL DEFAULT '20000',
		`type` int(11) unsigned NOT NULL DEFAULT '0',
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		KEY (`id`),
		UNIQUE KEY `md5` (`md5`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

	$array['urlstats'] = <<<SQL
		CREATE TABLE `{$name}_urlstats` (
		`id` int(11) unsigned NOT NULL Default 0,
		`md5text` varchar(32) NOT NULL DEFAULT '',
		`failures` int(11) unsigned NOT NULL DEFAULT '0',
		`downloads` int(11) unsigned NOT NULL DEFAULT '0',
		`code` int(11) unsigned NOT NULL DEFAULT '0',
		`last_try` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`last_success` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `md5text` (`md5text`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

	$array['datas'] = <<<SQL
		CREATE TABLE `{$name}_datas` (
		`id` int(11) unsigned NOT NULL Default 0,
		`extractor_id` int(11) unsigned NOT NULL DEFAULT '0',
		`url` varchar(255) NOT NULL DEFAULT '',
		`value` longblob,
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		UNIQUE KEY `id` (`id`,`extractor_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

	$array['pages'] = <<<SQL
		CREATE TABLE `{$name}_pages` (
		`id` int(11) unsigned NOT NULL DEFAULT '0',
		`md5` varchar(32) NOT NULL DEFAULT '',
		`url` varchar(255) NOT NULL DEFAULT '',
		`content` longblob,
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `md5` (`md5`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

	$array['fragments'] = <<<SQL
		CREATE TABLE `{$name}_fragments` (
		`id` int(11) unsigned NOT NULL DEFAULT '0',
		`md5` varchar(32) NOT NULL DEFAULT '',
		`count` int(11) unsigned NOT NULL DEFAULT 0,
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		KEY `id` (`id`),
		UNIQUE KEY `md5` (`md5`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

	return $array;
}

?>
