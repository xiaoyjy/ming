<?php

include (dirname(__DIR__)).'/src/init.php';

$which = isset($_GET['which']) ? $_GET['which'] : NULL;
$proxy = isset($_GET['proxy']) ? $_GET['proxy'] : 0;
$id    = isset($_GET['id']   ) ? $_GET['id'   ] : 0 ;

$extra = new CA_Ming_Crawler($which);
$r = $extra->fetch($id, $proxy);

echo json_encode($r);

?>
