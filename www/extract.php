<?php

include dirname(__DIR__).'/src/init.php';

$which = isset($_GET['which']) ? $_GET['which'] : NULL;
$id    = isset($_GET['id']   ) ? $_GET['id'   ] : 0 ;

$extra = new CA_Ming_Extractor($which);
$extra->process($id);

$table_pre = $which == NULL ? '' : $which."_";

$m = new CY_Model_Default($which.'datas');
$r = $m->mGet(['id' => $id]);

if(isset($r['data'][$id]))
{
	$data = $r['data'][$id];
	$data['value'] = msgpack_unserialize(gzuncompress($data['value']));
	echo json_encode(['errno' => 0, 'data' => $data]);
	exit;
}

echo json_encode(['errno' => -1]);
?>
