<?php

include dirname(__DIR__).'/src/init.php';

$name = isset($_GET['name']) ? $_GET['name'] : '';
$cmd  = isset($_GET['cmd'] ) ? $_GET['cmd' ] : 'count';
$msg  = isset($_GET['msg'] ) ? $_GET['msg' ] : '';

$xq = new CY_Util_mQueue();
$xq->init($name);

$v = 0;
switch($cmd)
{
case 'count':
	$v = $xq->count();
	break;

case 'get':
	$v  = $xq->get();
	break;

case 'clean':
	$v  = $xq->clean();
	break;

case 'add':
	$v = $xq->send($msg);
	break;
}

echo json_encode(['errno' => 0, 'data' => $v]);

?>
