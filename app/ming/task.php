<?php

class CA_Ming_Task
{
	protected $db;

	function __construct()
	{
		$this->db = new CY_Util_MySQL();
	}

	function add($task)
	{
		$title = cy_val($task, 'title', '');
		$name  = cy_val($task, 'name' , '');
		$host  = cy_val($task, 'host' , '');
		$url   = cy_val($task, 'url'  , '');
		$standalone = cy_val($task, 'standalone', 0);
		if(empty($name) || empty($title))
		{
			return cy_dt(10000, 'no name or title');
		}

		if(empty($host) && !empty($url))
		{
			$parts = parse_url($url);
			if(empty($parts))
			{
				return cy_dt(10001, 'no host name');
			}
		}

		if(empty($url) && !empty($host))
		{
			$url = 'http://'.$host;
		}

		if(empty($host))
		{
			return cy_dt(10001, 'no host name');
		}

		$now  = date('Y-m-d H:i:s');
		$site = ['name' => $name, 'title' => $title, 'host' => $host, 'url' => $url, 'standalone' => $standalone, 'ctime' => $now];
		$sm   = new CA_Model_Site();
		$r1   = $sm->add($site);
		if($r1['errno'] !== 0 || empty($r1['data']['id']))
		{
			return $r1;
		}

		$sid    = $r1['data']['id'];
		$parts  = parse_url($url);
		$filter = [];
		$filter['rule']    = $parts['scheme'].'://'.$host.'/.*?\.htm';
		$filter['site_id'] = $sid; 
		$filter['ctime']   = $now;
		$fm     = new CA_Model_Filter();
		$r2     = $fm->add($filter);
		// @TODO filter error

		$item = ['url' => $url, 'host' => $host, 'type' => 1, 'site_id' => $sid];
		$um   = new CA_Model_URL();
		$r3   = $um->add($item);

		$queue = 'urls';
		if($standalone)
		{
			$tables = $this->standlone_sqls($name);
			foreach($tables as $sql)
			{
				$r = $this->db->query($sql);
				//@TODO crate table error.
			}

			$this->supervise_bash($name);

			$queue = $name.'_urls';
		}

		if(!empty($r3['data']['insert_id']))
		{
			$uq = new CY_Util_mQueue();
			$uq->init($queue);
			$uq->send($r3['data']['insert_id']);
		}

		return $r1;
	}

	function supervise_bash($name)
	{
		if(empty($name))
		{
			return cy_dt(13000, 'Invalid spuervise task name');
		}

		$dirname = dirname(dirname(__DIR__));
		foreach(['crawlerd', 'filterd', 'extractord', 'deliveryd'] as $app)
		{
			system('mkdir -p '.$dirname.'/run/'.$name.'_'.$app);

			$cmd = "#!/bin/bash\n\ncd ../../\n\nsbin/$app $name\n";
			system("echo '$cmd' > ".$dirname.'/run/'.$name.'_'.$app.'/run');
			system("chmod +x ".$dirname.'/run/'.$name.'_'.$app.'/run');
		}

		return cy_dt(0, []);
	}

	function standlone_sqls($name = '')
	{
		if(!empty($name))
		{
			$name .= '_';
		}

		$array = [];
		$array['urls'] = <<<SQL
		CREATE TABLE `{$name}urls` (
		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		`site_id` int(11) unsigned NOT NULL DEFAULT 0,
		`md5` varchar(32) NOT NULL DEFAULT '',
		`url` varchar(255) NOT NULL DEFAULT '',
		`host` varchar(64) NOT NULL DEFAULT '',
		`weight` int(11) unsigned NOT NULL DEFAULT '5',
		`timeout` int(11) unsigned NOT NULL DEFAULT '20000',
		`type` int(11) unsigned NOT NULL DEFAULT '0',
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `sid` (`site_id`, `id`),
		UNIQUE KEY `md5` (`md5`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

		$array['urlstats'] = <<<SQL
		CREATE TABLE `{$name}urlstats` (
		`id` int(11) unsigned NOT NULL Default 0,
		`md5text` varchar(32) NOT NULL DEFAULT '',
		`failures` int(11) unsigned NOT NULL DEFAULT '0',
		`downloads` int(11) unsigned NOT NULL DEFAULT '0',
		`code` int(11) unsigned NOT NULL DEFAULT '0',
		`last_try` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`last_success` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `md5text` (`md5text`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

		$array['datas'] = <<<SQL
		CREATE TABLE `{$name}datas` (
		`id` int(11) unsigned NOT NULL Default 0,
		`rule_id` int(11) unsigned NOT NULL DEFAULT '0',
		`url` varchar(255) NOT NULL DEFAULT '',
		`value` longblob,
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		UNIQUE KEY `rid` (`rule_id`, `id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

		$array['pages'] = <<<SQL
		CREATE TABLE `{$name}pages` (
		`id` int(11) unsigned NOT NULL DEFAULT '0',
		`md5` varchar(32) NOT NULL DEFAULT '',
		`url` varchar(255) NOT NULL DEFAULT '',
		`content` longblob,
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `md5` (`md5`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

		$array['fragments'] = <<<SQL
		CREATE TABLE `{$name}fragments` (
		`id` int(11) unsigned NOT NULL DEFAULT '0',
		`md5` varchar(32) NOT NULL DEFAULT '',
		`count` int(11) unsigned NOT NULL DEFAULT 0,
		`ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY `id` (`id`),
		UNIQUE KEY `md5` (`md5`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

		return $array;
	}

}

?>
