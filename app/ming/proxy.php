<?php

class CA_Ming_Proxy
{
	protected $ht;
	protected $xq;
	protected $xm;

	function __construct()
	{
		$this->xm = new CA_Model_Proxy();

		$this->ht = new CY_Util_Curl();
		$this->xq = new CY_Util_mQueue();
		$this->xq->init('proxy');
	}

	function loop()
	{
loop:
		$curl_mutil_num = 50;

		$size = $this->xq->count();
		if($size < 2500)
		{
			$new    = (int)($size > 500);
			$number = 1000;
			$i = 0;
			foreach($this->fetch($number, $new) as $proxy)
			{
				$this->check($proxy);
				if($i++%$curl_mutil_num == 0)
				{
					$this->wait($curl_mutil_num);
				}
			}
		}
		else
		{
			$p = $this->xq->get('autoack');
			if($p['errno'] !== 0 || empty($p['data']['body']))
			{
				cy_log(CYE_ERROR, 'Can get proxy from mqueue, sleep 2 seconds and try again');
				sleep(2);
				goto loop;
			}

			$b    = explode("\t", $p['data']['body']);
			$time = isset($b[1]) ? (int)$b[1] : 0;
			if(time() - $time < 300 /* 5分钟没有被用过，重新检查一下 */)
			{
				sleep(1);
			}
			else
			{
				$this->check($b[0]);
			}
		}

		$this->wait();
		goto loop;
	} 

	function wait($num = 20)
	{
		do
		{
			$this->ht->recv([$this, 'callback'], 0.1);
		}
		while($this->ht->size() > $num);
	}

	function callback($proxy, $document)
	{
		if(isset($document['data']) && strpos($document['data'], 'Sitemap: http://www.qq.com/sitemap'))
		{
			$this->xq->send($proxy."\t".time());
			$valid = true;
		}
		else
		{
			$valid = false;
		}

		$this->xm->stats($proxy, $valid, (int)($document['total_time']*1000));
	}

	function check($proxy)
	{
		$url      = 'http://www.qq.com/robots.txt';
		$headers  = ['User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95'];
		$options  = ['timeout' => 10000, 'getinfo' => true, 'include' => true];
		$options['proxy'] = $proxy;

		$c = new CY_Driver_Http_Default($url, 'GET', $headers, $options);
		$this->ht->add($proxy, $c);
	}

	function fetch($number, $new = 1)
	{
		$url  = $_ENV['config']['proxy_fetch_url'];
		$url .= '&count='.$number;
		$url .= '&isNew='.$new;

		$ht = new CY_Util_Curl();
		$pxy  = $ht->fetch($url);
		if(empty($pxy))
		{
			cy_log(CYE_ERROR, "get proxy from $url failed.\n");
			yield '';
		}

		$arr = json_decode($pxy, true);
		$ips = $arr['ips'];
		cy_log(CYE_DEBUG, "found ips count=".count($ips));
		if(!empty($arr['ips'])) foreach($arr['ips'] as $row)
		{
			yield $row['address'].':'.$row['port'];
		}
	}
}

?>
