<?php

class CA_Ming_Delivery
{
	/* pages queue, urls queue */
	protected $xq;

	/* urls model, pages model, Filters model */
	protected $dm;

	protected $ec;

	function __construct($name)
	{
		$this->xq = new CY_Util_mQueue();
		$this->xq->init($name.'_data');	

		$this->dm = new CY_Model_Default($name.'_datas' );
		$this->ec = new CY_Model_Default('extractor');

		$this->curl = new CY_Util_Curl();
	}

	/**
	 * Loop
	 *
	 * read message from pages queue, and process one by one.
	 */
	function loop()
	{
		$this->xq->loop([$this, 'process']);
	}

	/**
	 * Process - main process functions.
	 *
	 * @param id pages table id
	 *
	 */
	function process($id)
	{
		if(empty($id)) return true;

		$_SERVER['REQUEST_URI'] = '';
		cy_log(CYE_DEBUG, "delivery id=$id process start.");

		list($eid, $url_id) = explode("_", $id);
		$r = $this->ec->mGet(['id' => $eid]);
		if(empty($r['data']))
		{
			return true;
		}

		$cfg      = current($r['data']);
		$callback = $cfg['callback'];
		$dt = $this->dm->mGet('extractor_id='.$eid." AND id=".$url_id);
		$value = [];
		foreach($dt['data'] as $row)
		{
			$id = max($row['id'], $id);
			$value = msgpack_unserialize(gzuncompress($row['value']));
			$value['url']    = $row['url'];
			$value['url_id'] = $row['id'];
			break;
		}

		if(empty($value))
		{
			return true;
		}
	
		$data = ['json' => json_encode($value)];
		$dt = $this->curl->fetch($callback, 'POST', [], ['data' => $data]);
		return true;
	}

}
