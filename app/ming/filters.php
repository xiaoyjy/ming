<?php

class CA_Ming_Filters
{
	/* pages queue, urls queue */
	protected $uq, $pq;

	/* urls model, pages model, Filters model */
	protected $um, $pm, $fm;

	function __construct($name = NULL)
	{
		$table_pre = $name ? $name.'_' : '';

		$this->uq = new CY_Util_mQueue();
		$this->pq = new CY_Util_mQueue();
		$this->uq->init($table_pre.'urls');
		$this->pq->init($table_pre.'filters');

		/* db */
		$this->um = new CY_Model_Default($table_pre.'urls'   );
		$this->tm = new CY_Model_Default($table_pre.'urlstats');
		$this->pm = new CY_Model_Default($table_pre.'pages'  );

		$this->fm = new CY_Model_Default('filters');
		$this->sm = new CY_Model_Default('sites'  );
	}

	/**
	 * Loop
	 *
	 * read message from pages queue, and process one by one.
	 */
	function loop()
	{
		$this->pq->loop([$this, 'process']);
	}

	/**
	 * Process - main process functions.
	 *
	 * @param id pages table id
	 *
	 */
	function process($id, $options = [])
	{
		if(empty($id))
		{
			return true;
		}

		$_SERVER['REQUEST_URI'] = '';
		cy_log(CYE_DEBUG, "filter id=$id process start.");

		$dt = $this->pm->mGet(['id' => $id]);
		if($dt['errno'] !== 0)
		{
			cy_log(CYE_ERROR, "model get pages error, id=$id ");
			return false;
		}

		if(empty($dt['data']))
		{
			cy_log(CYE_WARNING, "no pages data found, id=$id ");
			return true;
		}

		$du = $this->um->mGet(['id' => $id]);
		if(empty($du['data'][$id]))
		{
			cy_log(CYE_WARNING, "no url data found, id=$id ");
			return true;
		}

		$page  = current($dt['data']);
		$url   = $page['url'];
		if(empty($url))
		{
			cy_log(CYE_WARNING, "bad page data found, no url, id=$id ");
			return true;
		}

		$now   = date("Y-m-d H:i:s");
		$parts = parse_url($page['url']);
		$dir   = isset($parts['path']) ? dirname($parts['path']) : '';
		$host  = $parts['host'];
		$scheme= $parts['scheme'];
		$site_id = $du['data'][$id]['site_id'];

		/* used by log.php */
		$_SERVER['REQUEST_URI'] = $url;
		cy_log(CYE_DEBUG, "filter id=$id init url info OK.");

		$urls    = array();
		$content = gzuncompress($page['content']);
		/*
		if(is_object($content))
		{
			$content = $content->bin;
		}
		*/

		/* XML parser */
		if(strncasecmp('<?xml', $content, 4) === 0)
		{
			cy_log(CYE_DEBUG, "filter id=$id use XML parser");
			if(preg_match_all('#<loc>(.*?)</loc>#i', $content, $links))
			{
				$urls = $links[1];
			}
		}
		/* HTML parser */
		else
		{
			cy_log(CYE_DEBUG, "filter id=$id use HTML parser");

			$ex = new CA_Util_Extractor($id, $url);
			$ex->load($content);
			$urls = $ex->links($host, $dir, ['scheme' => $scheme]);
		}

		if(isset($options['print_all']))
		{
			print_r($urls);
		}

		if(empty($urls))
		{
			return true;                                                                                                        
		}

		/* get url filters by host name */
		$dt = $this->fm->mGet("`site_id`='$site_id' AND `enable`=1 AND `status`!=99");
		if(empty($dt['data']))
		{
			cy_log(CYE_WARNING, "no filters site config ready, skip it, host ".$host);
			return true;
		}

		$filters = $dt['data'];

		/*
		$dt = $this->sm->mGet(['host' => $host]);
		if(empty($dt['data']))
		{
			cy_log(CYE_WARNING, "No site ready, skip it, host ".$host);
			return true;
		}

		$site_id = current($dt['data'])['id'];
		*/

		/* stat page url. */
		$total    = count($urls);
		$effective= 0;
		$succ     = 0;
		$fail     = 0;
		$exists   = 0;

		/* just save urls we needs. */
		$selected= $md5s = $types = array();
		foreach($urls as $url)
		{
			$url = cy_url_normalize($url);
			foreach($filters as $filter)
			{
				$rule = str_replace('#', '\#', $filter['rule']);
				if(!preg_match("#^".$rule.'#i', $url, $m))
				{
					continue;
				}

				$md5  = md5($url);
				$item = [
					'site_id'=> $site_id,
					'url'    => $url,
					'md5'    => $md5,
					'host'   => $host,
					'weight' => $filter['weight'],
					//'timeout'=> $filter['timeout'],
					'type'   => $filter['type'],
					'ctime'  => $now
						];	

				$md5s[] = $md5;
				$selected[$md5] = $item;

				$effective++;
			}
		}

		if(isset($options['print']))
		{
			print_r($selected);
		}

		if(empty($selected))
		{
			cy_log(CYE_DEBUG, "pages id=$id is up to date");
			return true;
		}

		/* Update url insert times for all. */
		//$this->um->update(['md5' => $md5s], ['insert_times' => '+=1']);

		/* SELECT exists. 取到了可以减小写压力，取不到也没有关系。 */
		$dt = $this->um->mGet(['md5' => $md5s]);
		if(isset($dt['data']))
		{
			foreach($dt['data'] as $row)
			{
				$md5 = $row['md5'];
				unset($selected[$md5]);

				$exists++;
			}
		}

		/* Save url info and notify crawler to download it. */
		foreach($selected as $k => $row)
		{
			/* save url, save foreach, we need exactly insert_id  */
			$dt = $this->um->mSet([$row], ['update' => true, 'retry' => 2]);
			if($dt['errno'] !== 0)
			{
				$fail++;
				cy_log(CYE_ERROR, "insert urls info %s into database error.", $row['url']);
				continue;
			}

			/* start notify. */
			$insert_id = $dt['data']['insert_id'];
			if($insert_id)
			{
				$this->tm->mSet([['id' => $insert_id, 'ctime' => $now]], ['update' => true]);
				$dt = $this->uq->send($insert_id);
				if($dt['errno'] !== 0)
				{
					$fail++;
					cy_log(CYE_ERROR, "Notify page queue error, id=$insert_id %s", $row['url']);
					continue;
				}
			}

			$succ++;
		}

		cy_log(CYE_NOTICE, "filter pages id=$id total=$total effective=$effective exists=$exists succ=$succ fail=$fail");
		return true;
	}

}

?>
