<?php

class CA_Ming_Crawler
{
	protected $uq, $pq, $cq;
	protected $um, $pm, $xm;

	protected $ht;

	protected $proxy; /* SplQueue proxy queue item = host:port */
	protected $queue;
	protected $retry;

	protected $po;

	protected $urls, $tags;
	protected $sites;

	function __construct($name = NULL)
	{
		$table_pre = $name === NULL ? '' : $name.'_';

		$this->uq = new CY_Util_mQueue();
		$this->uq->init($table_pre.'urls');

		/* 用于 */
		$this->pq = new CY_Util_mQueue();
		$this->cq = new CY_Util_mQueue();
		$this->pq->init($table_pre.'filters');
		$this->cq->init($table_pre.'extract');

		/* proxy. */
		$this->proxy = new CY_Util_mQueue();
		$this->proxy->init('proxy');

		$this->queue = new SplQueue();
		$this->retry = new SplQueue();

		$this->pm = new CY_Model_Default($table_pre.'pages');
		$this->um = new CY_Model_Default($table_pre.'urls' );
		$this->tm = new CY_Model_Default($table_pre.'urlstats');
		$this->xm = new CA_Model_Proxy();

		$this->ht = new CY_Util_Curl();

		$this->urls = [];
		$this->tags = [];
		//$this->sites();
	}

	function stats($id, $row)
	{
		$code = $row['code'];
		$now  = date('Y-m-d H:i:s');
		$data = ['last_try' => $now, 'downloads' => '+=1', 'code' => $code];
		if($code == 200)
		{
			$data['last_success'] = $now;
		}
		else
		{
			$data['failures'] = '+=1';
		}

		if(isset($row['md5text']))
		{
			$data['md5text'] = $row['md5text'];
		}

		$dt = $this->tm->update(['id' => $id], $data);
		if($dt['errno'] !== 0)
		{
			cy_log(CYE_ERROR, "crawler id=$id update url status failed, skip it.");
		}
	}

	function ack($id)
	{
		if(isset($this->tags[$id]))
		{
			$this->uq->ack($this->tags[$id]);
		}

		if(isset($this->urls[$id]['code']))
		{
			$this->stats($id, $this->urls[$id]);
		}

		/* aviod memory leak. */
		unset($this->tags[$id]);
		unset($this->urls[$id]);
	}

	function shift_url()
	{
		$_SERVER['REQUEST_URI'] = '';

		/* 如果重试队列里有数据，先把它取干净 */
		while(!$this->retry->isEmpty())
		{
			$id = $this->retry->shift();

			/* retry 队列中可能有两个id相同的，对处理了一个，还有一个continue掉就行了. */
			if(empty($this->urls[$id]) || empty($this->urls[$id]['url']))
			{
				$this->ack($id);
				continue;
			}

			$retry = $this->urls[$id]['retry']++;
			$row   = $this->urls[$id];

			cy_log(CYE_WARNING, "crawler id=$id retry=$retry %s", $row['url']);
			if($retry > $_ENV['config']['crawler']['max_retry'])
			{
				cy_log(CYE_ERROR, "crawler id=$id %s retry=$retry reach max times, skip it.", $row['url']);

				$this->ack($id);
				$id = 0;
			}
			else
			{
				cy_log(CYE_DEBUG, "crawler id=$id %s start retry=$retry.", $row['url']);
				break;
			}
		}

		/* 如果重试队列里已经没有了，再从urls队列中拿. */
		if(empty($id))
		{
			$dt = $this->uq->get();
			if($dt['errno'] !== 0)
			{
				cy_log(CYE_ERROR, 'urls message queue error');
				return [0, null];
			}
			if(empty($dt['data']))
			{
				return [0, null];
			}

			$id = trim($dt['data']['body']);
			$this->tags[$id] = $dt['data']['tag'];
		}

		cy_log(CYE_DEBUG, "crawler id=$id process start.");
		$dt = $this->um->mGet(['id' => $id]);
		if($dt['errno'] !== 0)
		{
			cy_log(CYE_ERROR, "db model error query pages id=$id ");
			return [$id, null];
		}

		if(empty($dt['data'][$id]))
		{
			cy_log(CYE_WARNING, "no url found, id=$id skip it.");

			$this->ack($id);
			return [$id, null];
		}

		$row = $dt['data'][$id];
		if(empty($this->urls[$id]))
		{
			$row['retry']    = 0;
			$this->urls[$id] = $row;
		}

		$_SERVER['REQUEST_URI'] = $row['url'];
		return [$id, $row];
	}

	function save($id, $document)
	{
		$_SERVER['REQUEST_URI'] = $document['url'];

		$proxy = isset($document['proxy']) ? $document['proxy'] : '';
		cy_log(CYE_NOTICE, "crawler id=$id downloaded proxy=$proxy cost={$document['total_time']} ".
				"code={$document['http_code']} size={$document['size_download']} total_size={$document['download_content_length']}");

		if(empty($this->urls[$id]['url']))
		{
			cy_log(CYE_WARNING, 'bad url sets. id='.$id);
			return false;
		}

		$code = $document['http_code'];
		$this->urls[$id]['code'] = $code;
		if($code > 199 && $code < 400)
		{
			if(isset($document['proxy']))
			{
				if(isset($document['headers']['Response Code']))
				{
					$code = $document['headers']['Response Code'];
					if(400 > $code && $code > 300 && isset($document['headers']['Location']))
					{
						$location = $document['headers']['Location'];
						$u1 = parse_url($location);
						if(isset($u1['host']) && $u1['host'] != $this->urls[$id]['host'])
						{
							$this->urls[$id]['code'] = $code;
							$this->retry->push($id);

							$this->xm->stats($document['proxy'], false, (int)($document['total_time']*1000));
							cy_log(CYE_WARNING, "proxy={$document['proxy']} location to another site, give up."); 
							return false;
						}
					}

					if(strlen($document['data']) < 200)
					{
						$this->retry->push($id);

						$this->xm->stats($document['proxy'], false, (int)($document['total_time']*1000));
						cy_log(CYE_WARNING, "proxy={$document['proxy']} content size is too small");
						return false;
					}
				}

				$this->xm->stats($document['proxy'], true, (int)($document['total_time']*1000));
				$this->proxy->send($document['proxy']."\t".time());
			}
		}
		else
		{
			if(isset($this->urls[$id]['retry']))
			{
				/* 404, 500+的时候，不要试那么多次 */
				if($code === 404)
				{
					$this->urls[$id]['retry'] += 5;
				}
				else if($code > 399)
				{
					$this->urls[$id]['retry'] += 3;
				}

				$this->retry->push($id);
			}

			return false;
		}

		$r = $this->urls[$id];
		$url      = $r['url'];
		$type     = $r['type'];
		$now      = date("Y-m-d H:i:s");
		$contents = $document['data'];

		/* detect charset  */
		$charset  = cy_html_charset(substr($contents, 0, 1024));
		if(isset($document['content-type']))
		{	
			$cs = cy_html_charset($document['content-type']);
			if($cs != 'ISO-8859-1' || !$charset)
			{
				$charset = $cs;
			}
			else
			{
				$charset = cy_html_charset($contents);
			}
		}

		if($charset === 'ISO-8859-1')
		{
			cy_log(CYE_WARNING, "crawler id=$id %s unkown charset treat as iso-8859-1", $url);
		}

		/* Convert all contents into utf-8. */
		if($charset && $charset !== 'UTF-8' && $charset !== 'ISO-8859-1')
		{
			if($charset == 'GB2312' || $charset == 'GBK')
			{
				$charset = 'GB18030';
			}

			$contents = iconv($charset, "UTF-8//IGNORE", $contents);
			if(!$contents)
			{
				cy_log(CYE_ERROR, "crawler id=$id iconv content error.");

				$this->ack($id);
				return false;
			}

			$contents = preg_replace('/(<meta.*)charset(\s*?)=(\s*?)(["\']?)([\w-]+)(["\']?[ >])/i',
						'\1charset=\4utf-8\6', $contents);
		}

		/* Download success , start save.*/
		$data = array();
		//$data['_id']     = $id;
		$data['id']      = $id;
		$data['url']     = $url;
		$data['content'] = gzcompress($contents, 9);
		//$data['content'] = new MongoBinData($contents);
		$data['md5']     = $md5 = md5($contents);
		$data['ctime']   = $now;
		$this->urls[$id]['md5text'] = $md5;
		$dt = $this->pm->mSet([$data], ['update' => true]);
		if($dt['errno'] !== 0)
		{
			/* Error */
			cy_log(CYE_ERROR, "crawler id=$id insert contents failed");

			$this->ack($id);
			return false;
		}

		$insert_id = $dt['data']['insert_id'];
		cy_log(CYE_DEBUG, "crawler url_id=$id _id=$insert_id save success");

		$r1  = $this->pq->send($id);
		$tmp = " [urlq errno={$r1['errno']}]";
		/* type == 1, mean is at content page. */
		if($type == 1)
		{
			/* Notify extract queue */
			$r2  = $this->cq->send($id);
			$tmp.= " [extractq errno={$r2['errno']}]";
		}

		cy_log(CYE_DEBUG, "crawler id=$id save success, [pages id=".$insert_id.']'.$tmp);
		$this->ack($id);
		return true;
	}

	function fetch($id, $proxy)
	{
		$dt = $this->um->mGet(['id' => $id]);
		$url = isset($dt['data'][$id]['url']) ? $dt['data'][$id]['url'] : '';
		if(empty($url))
		{
			return cy_dt(1, "No url");
		}

		$this->urls[$id] = $dt['data'][$id];
		$opt = ['timeout' => 20000, 'getinfo' => true, 'include' => true];
		if($proxy)
		{
			do
			{
proxy_get:
				$p = $this->proxy->get('autoack');
				if($p['errno'] !== 0 || empty($p['data']['body']))
				{
					cy_log(CYE_ERROR, 'Can get proxy from mqueue, sleep 2 seconds and try again');
					sleep(2);
					goto proxy_get;
				}

				$b = explode("\t", $p['data']['body']);
				$time = isset($b[1]) ? (int)$b[1] : 0;
			}
			while(time() - $time > 3600);

			$opt['proxy'] = $b[0];
			cy_log(CYE_DEBUG, "crawler id=$id proxy={$opt['proxy']} url load success");
		}

		$hdr   = $_ENV['config']['header'];
		$hdr[] = 'Referer: '.$url;
		$c = new CY_Driver_Http_Default($url, 'GET', $hdr, $opt);
		$this->ht->add('default', $c);
		$r = $this->ht->get();
		if(isset($r['data']['default']['http_code']) && $r['data']['default']['http_code'] == 200)
		{
			$docs = $r['data']['default'];
			$docs['url'] = $url;
			$this->save($id, $docs);
		}

		return cy_dt(0, $r['data']['default']);
	}

	function loop($number = 512)
	{
		//$headers  = ['User-Agent: Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'];
		$headers  = $_ENV['config']['header'];
		$options  = ['timeout' => 20000, 'getinfo' => true, 'include' => true];

loop:
		list($id, $row) = $this->shift_url();
		if($id && $row)
		{
			$url = $row['url'];

			$hdr = $headers;
			$hdr[] = 'Referer: '.$url;

			do
			{
proxy_get:
				$p = $this->proxy->get('autoack');
				if($p['errno'] !== 0 || empty($p['data']['body']))
				{
					cy_log(CYE_ERROR, 'Can get proxy from mqueue, sleep 2 seconds and try again');
					sleep(2);
					goto proxy_get;
				}

				$b = explode("\t", $p['data']['body']);
				$time = isset($b[1]) ? (int)$b[1] : 0;
			}
			while(time() - $time > 3600);

			$opt = $options;
			$opt['proxy'] = $proxy = $b[0];
			$opt['timeout'] = $row['timeout'];
			cy_log(CYE_DEBUG, "crawler id=$id proxy=$proxy url load success");

			$c = new CY_Driver_Http_Default($url, 'GET', $hdr, $opt);
			$this->ht->add($id, $c);
		}

		do
			{
				$this->ht->recv([$this, 'save'], 0.1);
			}
		while($this->ht->size() > $number);

		cy_log(CYE_DEBUG, "crawler task end, proxys=".$this->proxy->count().
			" retrys=".$this->retry->count().
			" requests=".$this->ht->size().
			" urls=".count($this->urls).
			" tags=".count($this->tags));

		cy_log_id_renew();
		cy_stat_flush  ();
		if(empty($id))
		{
			usleep(500000);
		}

		if(memory_get_usage() > 104857600 /* 100*1024*1024 */ )
		{
			system('sudo gcore '.posix_getpid());
			cy_log(CYE_ERROR, 'memory usage exceed 100M, coredump & exit.');
			exit;
		}

		gc_collect_cycles();
		goto loop;
	}
}

?>
