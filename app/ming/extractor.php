<?php

/*
if(!class_exists('HTML5_Parser'))
{
	include CY_HOME.'/app/3rd/html5lib/HTML5/Parser.php';
}
*/

class CA_Ming_Extractor 
{
	/* pages queue, urls queue */
	protected $uq, $pq;

	/* urls model, pages model, Filters model */
	protected $um, $pm, $fm;

	protected $ec, $ac, $sc;

	function __construct($name = NULL)
	{
		$table_pre = $name ? $name.'_' : '';

		$this->uq = new CY_Util_mQueue();
		$this->uq->init($table_pre.'extract');

		$this->xq = new CY_Util_mQueue();
		$this->xq->init($table_pre.'data');	

		$this->pm = new CY_Model_Default($table_pre.'pages' );
		$this->dm = new CY_Model_Default($table_pre.'datas' );
		$this->um = new CY_Model_Default($table_pre.'urls'  );
		$this->cm = new CY_Model_Default($table_pre.'fragments');

		$this->fm = new CY_Model_Default('filters'  );
		$this->ec = new CY_Model_Default('rules'    );
		$this->ac = new CY_Model_Default('attrs'    );
		$this->sc = new CY_Model_Default('sites'    );
	}

	/**
	 * Loop
	 *
	 * read message from pages queue, and process one by one.
	 */
	function loop()
	{
		$this->uq->loop([$this, 'process']);
	}

	/**
	 * Process - main process functions.
	 *
	 * @param id pages table id
	 *
	 */
	function process($id)
	{
		if(empty($id)) return true;

		$_SERVER['REQUEST_URI'] = '';
		cy_log(CYE_DEBUG, "exctract id=$id process start.");

		$dt = $this->pm->mGet(['id' => $id]);
		if(empty($dt['data']))
		{
			cy_log(CYE_WARNING, "no pages data found, id=$id ");
			return true;
		}

		$page = current($dt['data']);
		if(empty($page['url']) || empty($page['content']))
		{
			cy_log(CYE_WARNING, "no pages url or content, id=$id ");
			return true;
		}

		$url   = $page['url'];
		$url_id= (int)$page['id'];
		$parts = parse_url($page['url']);

		$dir   = dirname($parts['path']);
		$host  = $parts['host'];
		$now   = date('Y-m-d H:i:s');

		/* used by log.php */
		$_SERVER['REQUEST_URI'] = $url;

		$dt = $this->um->mGet(['id' => $id]);
		if(empty($dt['data'][$id]))
		{
			cy_log(CYE_WARNING, "No url data found, id=$id ");
			return true;
		}

		$sitename  = $dt['data'][$id]['host'];
		$dt = $this->sc->mGet(['host' => $sitename]);
		if(empty($dt['data']))
		{
			cy_log(CYE_WARNING, "No site data found, site=$sitename, pageid=$id"); 
			$site = ['name' => $host, 'host' => $host];
		}
		else
		{
			$site   = current($dt['data']);
			$siteid = $site['id'];
		}

		cy_log(CYE_DEBUG, "exctract id=$id init url info OK.");
		$content = gzuncompress($page['content']);

		/* HTML parser */
		cy_log(CYE_DEBUG, "exctract id=$id use HTML parser");

		$ex = new CA_Util_Extractor($id, $url);
		$ex->load($content);
		$datas   = [];
		$configs = $this->configs($siteid, $url);

		//$tidy    = new tidy();
		//$content = $tidy->repairString($content);
		//$dom     = new DomDocument();
		//$dom     = simplexml_load_string($content, 'SimpleXMLElement', LIBXML_NOERROR);
		$xpath   = null;

		$messages = [];
		foreach($configs as $eid => $config)
		{
			$eid  = (int)$eid;
			$data = [];

			if(empty($config))
			{
				foreach(['id', 'title', 'content', 'time'] as $name)
				{
					$data[$name] = call_user_func([$ex, $name]);
				}
			}
			else
			{
				foreach($config as $name => $cfg)
				{
					switch($cfg['type'])
					{
					case 1:
						$source = $cfg['source'] === 1 ? $url : $content;
						$regex  = '/'.str_replace('/', '\/', $cfg['pattern']).'/sui';
						$value  = trim(cy_regex_extract($source, $regex, $cfg['choice']));
						break;

					case 2:
						if(!method_exists($ex, $cfg['callback']))
						{
							break;
						}

						$value = trim(call_user_func([$ex, $cfg['callback']])); 
						break;

					case 3:
						if($xpath == null)
						{
							/* Change from HTML5lib-php to HTML5-php */
							$html5   = new Masterminds\HTML5(['disable_html_ns' => true]);
							$dom     = $html5->loadHTML($content); 

							//$dom     = HTML5_Parser::parse($content);
							$xpath   = new DomXPath($dom);
						}

						$nodes = $xpath->query($cfg['pattern']);
						$value = ($nodes->length > 0) ? $nodes->item(0)->nodeValue : '';
						break;

					default:
						$value = '';
						break;

					}

					if(is_callable($cfg['filter']))
					{
						$value = call_user_func($cfg['filter'], $value);
					}


					$data[$name] = $value;
				}

			}

			$data = gzcompress(msgpack_pack($data), 9);
			$datas[] = [ 'rule_id' => $eid, 'url' => $url, 'id' => $url_id, 'value' => $data, 'ctime' => $now];
			$messages[] = $eid.'_'.$url_id;
		}

		if(!empty($datas))
		{
			$dt = $this->dm->mSet($datas, ['update' => true]);
			if($dt['errno'] !== 0)
			{
				cy_log(CYE_ERROR, "exctract id=$id %s store data error.", $url);
				return false;
			}
		}

		foreach($messages as $message)
		{
			$this->xq->send($message);
		}

		cy_log(CYE_NOTICE, "exctract id=$id %s finished.", $url);
		return true;
	}

	function configs($siteid, $url)
	{
		//$dt = $this->ec->mGet(['site_id' => $siteid, 'enable' => 1], ['key' => 'id']);
		$dt = $this->ec->mGet('site_id='.(int)$siteid.' AND enable=1 AND status=0', ['key' => 'id']);
		if(empty($dt['data']))
		{
			return [];
			//return [['title' => NULL, 'content' => NULL, 'time' => NULL]];
		}

		$rr = array_column($dt['data'], 'id');
		if(empty($rr))
		{
			return [];
		}

		$r1 = $this->ac->mGet('rule_id IN ("'.implode('","', $rr).'") AND enable=1 AND status=0', ['key' => 'id']);

		$configs = [];
		foreach($dt['data'] as $eid => $row)
		{
			$pattern = '/'.str_replace('/', '\/', $row['pattern']).'/';
			if(!preg_match($pattern, $url))
			{
				continue;
			}

			$config = [];
			foreach($r1['data'] as $attr)
			{
				if($eid == $attr['rule_id'] && $attr['status'] == 0)
				{
					$config[$attr['name']] = $attr;
				}
			}

			$configs[$eid] = $config;
		}

		return $configs;
	}

	function content($ex, $content)
	{
		$txt = $ex->content();
		if($txt == '')
		{
			return $txt;
		}

		$md5 = md5($txt);
		$max = 1;
		$num = 0;
		$row['md5'] = $md5;
		$dt = $this->cm->mGet($row);
		if(empty($dt['data']))
		{
			$row['ctime'] = date('Y-m-d H:i:s');
			$row['count'] = 1;
			$r = $this->cm->mSet([$row]);
		}
		else
		{
			$num = $dt['data'][$md5]['count'];
			$r = $this->cm->max('count');
			if(!empty($r['data']))
			{
				$max = $r['data'][0]['count'];
			}

			$r = $this->cm->update($row, ['count'=>'+=1']);
		}

		if(($max > 4 && $num/$max>0.5) || $min > 20)
		{
			#echo $max, "\t", $num, "\n";
			$ex->black($md5);
			return $this->content($ex, $content);
		}

		return $txt;
	}

}

?>
