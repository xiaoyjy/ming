<?php

class CA_Model_Rule
{
	protected $rm;

	function __construct()
	{
		$this->rm = new CY_Model_Default('rules');
	}

	function add($rule)
	{
		$site_id = cy_val($rule, 'site_id'  , 0);
		if(empty($site_id))
		{
			return cy_dt(10000, 'unknown url or site_id');
		}

		if(empty($rule['pattern']))
		{
			return cy_dt(10001, 'unknown rule');
		}

		empty($rule['enable']) && $rule['enable'] = 1;
		empty($rule['ctime' ]) && $rule['ctime' ] = date('Y-m-d H:i:s');
		$r1 = $this->rm->mSet([$rule], ['update' => true]);
		return $r1;
	}

}

?>
