<?php

class CA_Model_URL
{
	protected $um, $um_ex;
	protected $prefix = '';

	function __construct($prefix = '')
	{
		if(!empty($prefix))
		{
			$this->prefix = $prefix.'_';
		}

		$this->um    = new CY_Model_Default($this->prefix.'urls');
		$this->um_ex = new CY_Model_Default($this->prefix.'urlstats');
	}


	function add($item)
	{
		if(empty($item['url']) || empty($item['site_id']))
		{
			return cy_dt(12000, 'empty url or site_id');
		}

		$now = date('Y-m-d H:i:s');
		$item['ctime'] = $now;
		$item['md5'  ] = md5($item['url']);
		if(empty($item['host']))
		{
			$parts = parse_url($item['url']);
			$item['host'] = $parts['host'];
		}

		$r = $this->um->mSet([$item], ['update' => true]);
		if(!empty($r['data']['insert_id']))
		{
			$stat = ['id' => $r['data']['insert_id'], 'ctime' => $now];
print_r($stat);
			$r1 = $this->um_ex->mSet([$stat], ['update' => true]);
print_r($r1);
		}
		
		return $r;
	}
}


?>
