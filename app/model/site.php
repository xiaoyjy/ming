<?php

class CA_Model_Site
{
	protected $sm;

	function __construct()
	{
		$this->sm = new CY_Model_Default('sites');
		$this->fm = new CY_Model_Default('filter');
	}


	function add($site)
	{
		if(empty($site['name']) || empty($site['host']) || empty($site['url']))
		{
			return cy_dt(11000, 'name or host or url is empty');
		}

		$name = $site['name'];
		$host = $site['host'];
		$url  = $site['url' ];
		$r1   = $this->sm->mSet([$site], ['update' => true]);
		$r2   = $this->sm->mGet(['name' => $name]);
		if(empty($r2['data'][$name]['id']))
		{
			return cy_dt(10003, 'add site failed');
		}

		//$r3 = $this->fm->mGet(['host' => $host]);
		return cy_dt(0, ['id' => $r2['data'][$name]['id']]);
	}

}

?>
