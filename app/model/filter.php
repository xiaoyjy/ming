<?php

class CA_Model_filter
{
	protected $fm;

	function __construct()
	{
		$this->fm = new CY_Model_Default('filters');
	}

	function add($filter)
	{
		$site_id = cy_val($filter, 'site_id'  , 0);
		if(empty($site_id))
		{
			return cy_dt(10000, 'unknown url or site_id');
		}

		if(empty($filter['rule']))
		{
			return cy_dt(10001, 'unknown rule');
		}

		empty($filter['enable']) && $filter['enable'] = 1;
		empty($filter['weight']) && $filter['weight'] = 5;
		empty($filter['ctime' ]) && $filter['ctime' ] = date('Y-m-d H:i:s');

		$r1 = $this->fm->mSet([$filter], ['update' => true]);
		if(isset($r1['data']['insert_id']) && $r1['data']['insert_id'] == 0)
		{
			$r2 = $this->fm->mGet(['rule' => $filter['rule']]);
			$r1['data']['insert_id'] = $r2['data'][0]['id'];
		}

		return $r1;
	}

}

?>
