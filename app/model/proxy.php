<?php

class CA_Model_Proxy
{
	protected $xm;

        function __construct()
        {
                $this->xm = new CY_Model_Default('proxy');
        }


	function stats($proxy, $valid, $time)
	{
		$now  = date('Y-m-d H:i:s'); 
		$succ = (int)$valid;
		$fail = (int)(!$valid);
		$r = $this->xm->mGet(['addr' => $proxy]); 
		if(empty($r['data']))
		{
			$item = ['addr' => $proxy, 'succ' => $succ, 'fail' => $fail, 'ctime' => $now, 'latency' => $time];
			if($fail)
			{
				$item['deadtime1' ] = $now;
				$item['deadcount'] = 1;
			}

			$r1 = $this->xm->mSet([$item]);
		}
		else
		{
			$old  = isset($r['data'][$proxy]) ? $r['data'][$proxy] : [];
			$item = ['latency' => '+='.$time];
			$succ && $item['succ'] = '+=1';
			if($fail)
			{
				$item['fail']      = '+=1';
				$item['deadtime2'] = $now;
				$item['deadcount'] = '+=1';
				if($old['deadcount'] == 0)
				{
					$item['deadtime1'] = $now;
				}
			}
			else
			{
				$item['deadcount'] = 0;
			}

			$r1 = $this->xm->update(['addr' => $proxy], $item);
		}

		return $r1;
	}


}


?>
