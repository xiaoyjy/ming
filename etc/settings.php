<?php

/***/
$_ENV['config']['log'] = array
(
 'path'  => dirname(CY_LIB_PATH).'/log',
 'name'  => 'ming',
 'level' => 8,
 'fflush' => true 
);

$_ENV['config']['stat_max_line'] = 256;
$_ENV['config']['xhprof_enable'] = 0;
$_ENV['config']['unlink_log']    = 0;

$_ENV['config']['timeout'] = array
(
 'redis_connect' => 0.2,
 'redis_read' => 0.4,

 'mongo_connect' => 0.1,
 'mongo_read' => 0.9,

 'amqp_read' => 10,
 'amqp_write' => 0.5,
 'amqp_connect' => 0.5,

 'mysql_connect' => 1,
 'mysql_read' => 2,

 'http_connect' => 0.2,
 'http_read' => 0.5,

 'net_default' => 0.6,
);

$_ENV['config']['crawler'] = array
(
 'max_retry' => 10
);

//*
// Host: view.qq.com
// Connection: keep-alive
// Accept: text/css,*/*;q=0.1
// User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safari/537.36
// Referer: http://view.news.qq.com/intouchtoday/index.htm?2590
// Accept-Encoding: gzip,deflate,sdch
// Accept-Language: en-US,en;q=0.8
// Cookie: RK=p0zOJPA+HN; luin=o0171689323; lskey=0001000090a0073f3ca458a0a80d927d7775ce034d55762efa090360076a4ad85560d88b0048f5ccae79607c; pgv_pvi=2285630464; ptui_loginuin=171689323; pt2gguin=o0171689323; ptcz=8bc5b10332c046beb7d7f0d226f8be54df859af2f952a34f44622f0a775631db; pgv_info=ssid=s439062198; pgv_pvid=5637543880; o_cookie=171689323

//Host: www.qq.com
//Connection: keep-alive
//Cache-Control: max-age=0
//If-Modified-Since: 0
//User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.69 Safari/537.36
//Accept: */*
//Referer: http://www.qq.com/
//Accept-Encoding: gzip,deflate,sdch
//Accept-Language: en-US,en;q=0.8

$_ENV['config']['header'] = array
(
 'Connection: keep-alive',
 'Cache-Control: max-age=0',
 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
 'Accept-Encoding: gzip,deflate,sdch',
 'Accept-Language: en-US,en;q=0.8',
 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36',
// 'Cookie:Hm_lvt_55b574651fcae74b0a9f1cf9c8d7c93a=1415000542; bdshare_firstime=1415000542445; pma_lang=zh_CN; pmaUser-1=P2O9kkI237WxnKBqkNnbsA%3D%3D; sid=6cdcfKIPxjpn7Y7jQQW4%2FuWtLVflOh9PdhrDPqXallGOOsJxHjT3BGtZM5Dz; JSESSIONID=64E647C80F13C39872C18AB715DAAF64; seraph.confluence=1638405%3A0d59d7ec009c62ca1277304eaf5fc8c14d97a351; confluence-sidebar.width=296'
);


?>
