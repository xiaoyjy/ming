-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pages
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `filters`
--

DROP TABLE IF EXISTS `filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(64) NOT NULL DEFAULT '',
  `rule` varchar(255) NOT NULL DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  `enable` tinyint(4) NOT NULL DEFAULT '1',
  `timeout` int(11) unsigned NOT NULL DEFAULT '20000',
  `project` varchar(64) NOT NULL DEFAULT '',
  `weight` int(11) unsigned NOT NULL DEFAULT '5',
  `ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filters`
--

LOCK TABLES `filters` WRITE;
/*!40000 ALTER TABLE `filters` DISABLE KEYS */;
INSERT INTO `filters` VALUES (1,'www.waimai.com','http://www.waimai.com/food/\\d+',1,1,20000,'narhao',5,'2013-09-18 18:06:39','2013-09-18 10:08:18'),(2,'www.waimai.com','http://www.waimai.com/store/\\d+',1,1,20000,'narhao',5,'2013-09-18 18:07:13','2013-09-18 10:07:13'),(3,'www.xiaomishu.com','http://\\w+.xiaomishu.com/shop/\\w+\\d',1,0,20000,'narhao',5,'2013-09-24 22:44:22','2013-10-07 11:24:24'),(4,'www.mafengwo.cn','http://www.mafengwo.cn/i/.+',1,1,20000,'che',8,'2013-09-25 00:29:57','2013-09-24 16:29:57'),(5,'www.mafengwo.cn','http://www.mafengwo.cn/travel-scenic-spot/.+',0,1,20000,'che',8,'2013-09-25 00:30:59','2013-09-24 16:30:59'),(6,'www.mafengwo.cn','http://www.mafengwo.cn/yj/.+',0,1,20000,'che',8,'2013-09-25 00:32:05','2013-09-24 16:32:05'),(7,'www.mafengwo.cn','http://www.mafengwo.cn/mdd/detail.php.+',0,1,20000,'che',8,'2013-09-25 11:24:02','2013-09-25 03:24:02'),(8,'www.mafengwo.cn','http://www.mafengwo.cn/poi/.+',0,1,20000,'che',8,'2013-09-25 11:25:10','2013-09-25 03:25:10'),(9,'www.mafengwo.cn','http://www.mafengwo.cn/u/.+',0,1,20000,'che',8,'2013-09-25 11:41:46','2013-09-25 03:41:46'),(10,'waimai.taobao.com','http://waimai.taobao.com/shop_detail.htm.+',1,1,20000,'',5,'2013-10-07 22:03:35','2013-10-08 07:22:31'),(11,'waimai.taobao.com','http://waimai.taobao.com/item.htm.+',1,1,20000,'',7,'2013-10-07 22:03:35','2013-10-08 07:22:25'),(12,'waimai.taobao.com','http://waimai.taobao.com/shop_list.htm.+',0,1,20000,'',5,'2013-10-07 22:03:35','2013-10-07 14:05:22'),(13,'waimai.taobao.com','http://waimai.taobao.com/shop_notice.htm.+',1,1,20000,'',5,'2013-10-07 22:03:35','2013-10-07 14:05:22'),(14,'pinyin.sogou.com','http://pinyin.sogou.com/dict/list.php\\?c=\\d+.*',0,1,20000,'',5,'2013-10-13 12:15:17','2013-10-13 04:39:54'),(15,'pinyin.sogou.com','http://download.pinyin.sogou.com/dict/download_cell.php?.*',1,1,300000,'',5,'2013-10-13 12:21:39','2013-10-13 05:15:56');
/*!40000 ALTER TABLE `filters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-22 23:05:11
