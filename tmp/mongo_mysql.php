<?php

include dirname(__DIR__)."/src/init.php";

$m = new CY_Model_Mongo('oldpages');
$t = new CY_Model_Default('pages');

for($i = 0; $i < 1809445; $i += 1000)
{
	$d = [];
	for($j = $i; $j < $i + 1000; $j++)
	{
		$d[] = (string)($i + $j);
	}

$t1 = microtime(true);
	$dt = $m->mGet(['url_id' => ['$in' => $d]]);
	if(empty($dt['data']))
	{
		continue;
	}

$t2 = microtime(true);

	$datas = [];
	$lists = $dt['data'];
	foreach($lists as $row)
	{
		if(empty($row['content']))
		{
			continue;
		}

		if(!is_object($row['content']))
		{
			$content = gzcompress($row['content'], 9);
			$row['content'] = ($content);
			$row['md5']     = md5($content);
		}
		else
		{
			$content = $row['content']->bin;
			$content = gzcompress($content, 9);
			$row['content'] = $content;
			$row['md5']     = md5($content);
		}

		//$row['_id'] = (int)$row['url_id'];
		unset($row['_id']);
		$datas[] = $row;
	}

$t3 = microtime(true);
	if(!empty($datas))
	{
		$t->mSet($datas);
	}
$t4 = microtime(true);

echo ($t2 - $t1), "\t", ($t3 - $t2), "\t", ($t4 - $t3), "\n";

}

?>
