-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pages
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attrs`
--

DROP TABLE IF EXISTS `attrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attrs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `extractor_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `patten` varchar(255) NOT NULL DEFAULT '',
  `choice` tinyint(4) NOT NULL DEFAULT '0',
  `callback` varchar(64) NOT NULL DEFAULT '',
  `filter` varchar(64) NOT NULL DEFAULT '',
  `extract_by_url` tinyint(4) NOT NULL DEFAULT '0',
  `ctime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attrs`
--

LOCK TABLES `attrs` WRITE;
/*!40000 ALTER TABLE `attrs` DISABLE KEYS */;
INSERT INTO `attrs` VALUES (1,1,'id','http://www.mafengwo.cn/i/(.+)\\.',1,'','',1,'2013-09-26 14:04:19','2013-10-06 16:45:49'),(2,2,'dishes','var jsonListData =(.*?);\\s',1,'','',0,'0000-00-00 00:00:00','2013-10-06 16:35:29'),(3,2,'id','http://\\w+.xiaomishu.com/shop/(\\w+) ',1,'','',1,'0000-00-00 00:00:00','2013-10-06 16:36:03'),(5,1,'uid','<meta\\s*name=\"author\"\\s*content=\"(\\d+)',1,'','',0,'0000-00-00 00:00:00','2013-10-06 16:36:49'),(6,1,'username','<meta\\s*name=\"author\"\\s*content=\"\\d+,([^\">]*?)\">',1,'','',0,'0000-00-00 00:00:00','2013-10-08 12:53:13'),(7,1,'vote_count','<div id=\"topvote\\d+\" class=\"num\">(\\d+)<',1,'','',0,'0000-00-00 00:00:00','2013-10-06 16:37:54'),(8,3,'id','http://waimai.taobao.com/shop_notice.htm.*?shopid=(\\d+)',1,'','',1,'2013-09-26 14:04:19','2013-10-06 16:45:49'),(9,4,'id','http://waimai.taobao.com/item.htm.*?[&|\\?]id=(\\d+)',1,'','',1,'2013-09-26 14:04:19','2013-10-06 16:45:49'),(10,4,'content','<.-- 宝贝详情:start -->(.*?)<.-- 宝贝详情:end -->',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:36:27'),(11,4,'address','<div class=\"store-add\" title=\"(.*?)\">',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:27:45'),(12,4,'phone','<div class=\"store-tel\".*?<span>(.*?)</span>',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:27:56'),(13,4,'supply_fee','<b>配 送 费：</b>\\s+<span class=\"highlight\">(.*?)</span>',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:46:43'),(14,4,'supply_fee_min','<b>起送金额：</b>\\s+<span class=\"highlight\">(.*?)</span>',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:46:33'),(15,4,'supply_time','<b>配送时间：</b>\\s+<span class=\"highlight\">(.*?)</span>',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:46:59'),(16,4,'supply_area','<b>配送范围：</b>\\s+<span>(.*?)</span>',1,'','',0,'2013-09-26 14:04:19','2013-10-08 09:49:58'),(17,1,'crumb','<div class=\"crumb\"><strong></strong>(.*)</div>',0,'','',0,'2013-10-22 21:45:48','2013-10-22 13:45:48');
/*!40000 ALTER TABLE `attrs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-10-22 23:04:28
