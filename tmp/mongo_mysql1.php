<?php

include dirname(__DIR__)."/src/init.php";

$_ENV['config']['timeout']['mongo_read'] = 10; 
$_ENV['config']['timeout']['mysql_read'] = 2;

class CY_Util_Mongo1
{
        protected $config;
        protected $mongo;

        protected $c;

        function __construct($config = NULL)
        {
                $this->config = empty($config) ? $_ENV['config']['mongo_ming'] : $config;
                $this->connect();
        }

        function connect()
        {
                if(!$this->mongo)
                {
                        $i = array_rand($this->config);
                        $c = $this->config[$i];
                        $this->c = $c;

                        try
                        {
                                $opt = [];
                                $opt['connectTimeoutMS'] = $_ENV['config']['timeout']['mongo_connect'] * 1000;
                                $opt['socketTimeoutMS' ] = $_ENV['config']['timeout']['mongo_read'   ] * 1000;
                                $opt['connect']          = false;
                                $this->mongo = new MongoClient($c['uri'], $opt);
                        }
                        catch(Exception $e)
                        {
                                $this->mongo = NULL;
                                cy_log(CYE_ERROR, $e->getMessage());
                        }
                }

		return $this->mongo;
        }
}

$t1 = microtime(true);

$mo = new CY_Util_Mongo1();
$mp = $mo->connect();

$i = 0;

$rows = [];
$t = new CY_Model_Default('pages');

$id = new MongoId("5249a6f080d4c5ed3f8b4b48");
$it = $mp->ming->oldpages->find(['_id' => ['$gt' => $id]]);

foreach($it as $row)
{
	if($i++ % 1000 == 0)
	{
		$t2 = microtime(true);
		$t->mSet($rows);
		$t3 = microtime(true);


		echo $t2 - $t1,  "\t", $t3 - $t2, "\t", "\n";	
		$t1 = $t2;

		$rows = [];
	}

	if(empty($row['content']))
	{
		continue;
	}

	if(!is_object($row['content']))
	{
		$content = gzcompress($row['content'], 9);
		$row['content'] = ($content);
		$row['md5']     = md5($content);
	}
	else
	{
		$content = $row['content']->bin;
		$content = gzcompress($content, 9);
		$row['content'] = $content;
		$row['md5']     = md5($content);                                                                                    
	}

	//$row['_id'] = (int)$row['url_id'];
	unset($row['_id']);

	$rows[] = $row;
}

?>
