<?php

include dirname(__DIR__)."/src/init.php";

$m = new CY_Model_Mongo('oldpages');
$t = new CY_Model_Mongo('pages');

for($i = 205692; $i < 1809445; $i += 1000)
{
	$d = [];
	for($j = $i; $j < $i + 1000; $j++)
	{
		$d[] = (string)($i + $j);
	}

	$dt = $m->mGet(['url_id' => ['$in' => $d]]);
	if(empty($dt['data']))
	{
		continue;
	}

	$datas = [];
	$lists = $dt['data'];
	foreach($lists as $row)
	{
		if(empty($row['content']))
		{
			continue;
		}

		if(!is_object($row['content']))
		{
			$content = gzcompress($row['content'], 9);
			$row['content'] = new MongoBinData($content);
		}
		else
		{
			$content = $row['content']->bin;
			$content = gzcompress($content, 9);
			$row['content'] = new MongoBinData($content);
		}

		$row['_id'] = (int)$row['url_id'];
		unset($row['url_id']);

		$datas[] = $row;
	}

	if(!empty($datas))
	{
$t1 = microtime(true);
		$t->mSet($datas);
echo microtime(true) - $t1, "\n";
	}
}

?>
