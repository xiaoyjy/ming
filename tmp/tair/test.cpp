#include <tair_client_api.hpp>
#include <data_entry.hpp>

#include <cstdio>

using namespace tair;
using namespace tair::common;

int main()
{
	tair_client_api* tair = new tair_client_api();

	tair->set_log_level("ERROR");
	if(!tair->startup("127.0.0.1", NULL, "group_1"))
	{
		printf("error connect.");
	}

	char buffer[1000] = "----------------";

	data_entry key((const char*)"abc", false);
	data_entry value((const char*)buffer, false);

	int ret = tair->put(0, key, value, 100000, 0);
printf("ret=%d\n", ret);

	data_entry *data = NULL;
	
	ret = tair->get(0, key, data);
printf("ret=%d data=%s len=%d\n", ret, data->get_data(), data->get_size());

	delete tair;

}
