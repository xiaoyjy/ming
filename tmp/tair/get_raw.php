<?php

include dirname(dirname(__DIR__)).'/src/init.php';

define("PACKAGE_FLAG", 0x6D426454);

/* 16 bytes */
$global = [
	"flag" => [CY_TYPE_UINT32, PACKAGE_FLAG],
	"chid" => [CY_TYPE_UINT32, 1],
	"code" => [CY_TYPE_UINT32, 2 /* PCODE */],
	"size" => [CY_TYPE_UINT32, 0] /* Body Length of the request */
	];


/* 9 bytes. */
$header = [
	"flag"    => [CY_TYPE_UINT8 , 0],
	"area"    => [CY_TYPE_UINT16, 0],
	"count"   => [CY_TYPE_UINT32, 1],
	];


/* 40bytes + body */
$data_entry = [
 	"merged" => [CY_TYPE_UINT8 , 0],
	"area"   => [CY_TYPE_UINT32, 0],
	"flag1"  => [CY_TYPE_UINT16, 0],

	/* meta data start, 29 bytes. */
	"magic"    => [CY_TYPE_UINT16, 0],
	"checksum" => [CY_TYPE_UINT16, 0],
	"keysize"  => [CY_TYPE_UINT16, 0],
	"version"  => [CY_TYPE_UINT16, 0],
	"prefixsize" => [CY_TYPE_UINT32, 0],
	"valsize" => [CY_TYPE_UINT32, 0],
	"flag2"   => [CY_TYPE_UINT8, 0],
	"cdate"   => [CY_TYPE_UINT32, 0],
	"mdate"   => [CY_TYPE_UINT32, 0],
	"edate"   => [CY_TYPE_UINT32, 0],
	/* meta_data end */

	"size"   => [CY_TYPE_UINT32, 0],
	"body"   => [CY_TYPE_STRING, ""] 
];

$key = $data_entry;
$key['size'] = [CY_TYPE_UINT32, 3];
$key['body'] = [CY_TYPE_STRING, '1bc'];

$global["size"] = [CY_TYPE_UINT32, 7 + 40 + 3];

$inputs = [
 	"global" => [CY_TYPE_OBJECT, $global],
	"header" => [CY_TYPE_OBJECT, $header],
	"key"    => [CY_TYPE_OBJECT, $key],
//	"value"  => [CY_TYPE_OBJECT, $val]
];

$c_global = [
       [CY_TYPE_UINT32, "flag"],
       [CY_TYPE_UINT32, "chid"],
       [CY_TYPE_UINT32, "code" /* PCODE */],
       [CY_TYPE_UINT32, "size"] /* Body Length of the request */
      ];

$c_data = [
	[CY_TYPE_STRING, 'meta', 36],
	[CY_TYPE_UINT32, 'size'],
	[CY_TYPE_STRING, 'data', 'size']
];

$c_pair = [
	[CY_TYPE_OBJECT, 'key', $c_data],
	[CY_TYPE_OBJECT, 'val', $c_data]
];

$c_lists = [
	[CY_TYPE_UINT32, "count"],
	[CY_TYPE_OBJECT, 'list'  , $c_pair, 'count']
]; 

$config = [
	[CY_TYPE_OBJECT, 'global', $c_global],
	[CY_TYPE_UINT32, "version"],
	[CY_TYPE_COND32, NULL, [0 => $c_lists, CY_COMMON_COND => NULL]],
/*
	[CY_TYPE_UINT32, "code"],
	[CY_TYPE_UINT32, "count"],
	[CY_TYPE_OBJECT, 'list'  , $c_pair, 'count']
*/
];

$bindata = cy_pack($inputs);
print_r(str_split(bin2hex($bindata), 32));

$net = new CY_Util_Net();
$net->prepare('put1', ['server' => '127.0.0.1:5191', 'body' => $bindata]);

$bin = $net->get();
print_r($bin);

$dt  = (cy_unpack($bin['data']['put1']['data'], $config));

print_r($dt);
echo $dt['errno'] - 4294967296;

?>
