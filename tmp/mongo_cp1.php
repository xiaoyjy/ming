<?php

include dirname(__DIR__)."/src/init.php";

class CY_Util_Mongo1
{
        protected $config;
        protected $mongo;

        protected $c;

        function __construct($config = NULL)
        {
                $this->config = empty($config) ? $_ENV['config']['mongo_ming'] : $config;
                $this->connect();
        }

        function connect()
        {
                if(!$this->mongo)
                {
                        $i = array_rand($this->config);
                        $c = $this->config[$i];
                        $this->c = $c;

                        try
                        {
                                $opt = [];
                                $opt['connectTimeoutMS'] = $_ENV['config']['timeout']['mongo_connect'] * 1000;
                                $opt['socketTimeoutMS' ] = $_ENV['config']['timeout']['mongo_read'   ] * 1000;
                                $opt['connect']          = false;
                                $this->mongo = new MongoClient($c['uri'], $opt);
                        }
                        catch(Exception $e)
                        {
                                $this->mongo = NULL;
                                cy_log(CYE_ERROR, $e->getMessage());
                        }
                }

		return $this->mongo;
        }
}

$t1 = microtime(true);

$mo = new CY_Util_Mongo1();
$mp = $mo->connect();

$i = 0;

$it = $mp->ming->oldpages->find();
foreach($it as $v)
{
	if($i++ % 1000 == 0)
	{
		$t2 = microtime(true);

		echo $t2 - $t1, "\n";
		
		$t1 = $t2;
	}

}

$m = new CY_Model_Mongo('oldpages');
$t = new CY_Model_Mongo('pages');

/*
for($i = 205692; $i < 1809445; $i += 1000)
{
	$d = [];
	for($j = $i; $j < $i + 1000; $j++)
	{
		$d[] = (string)($i + $j);
	}

	$dt = $m->mGet(['url_id' => ['$in' => $d]]);
	if(empty($dt['data']))
	{
		continue;
	}

	$datas = [];
	$lists = $dt['data'];
	foreach($lists as $row)
	{
		if(empty($row['content']))
		{
			continue;
		}

		if(!is_object($row['content']))
		{
			$content = gzcompress($row['content'], 9);
			$row['content'] = new MongoBinData($content);
		}
		else
		{
			$content = $row['content']->bin;
			$content = gzcompress($content, 9);
			$row['content'] = new MongoBinData($content);
		}

		$row['_id'] = (int)$row['url_id'];
		unset($row['url_id']);

		$datas[] = $row;
	}

	if(!empty($datas))
	{
		$t->mSet($datas);
	}
}

*/
?>
