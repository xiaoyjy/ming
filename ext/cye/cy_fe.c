/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2012 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id: header 321634 2012-01-01 13:15:04Z felipe $ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"

#include "php_cye.h"
#include "hash/hashtable.h"
#include "cye.h"

#include <SAPI.h>
#include <dlfcn.h>

ZEND_DECLARE_MODULE_GLOBALS(cye)

/* True global resources - no need for thread safety here */
static int le_cye;

/* {{{ cye_functions[]
 *
 * Every user visible function must have an entry in cye_functions[].
 */
const zend_function_entry cye_functions[] = {
        PHP_FE(cy_i_init , NULL)
        PHP_FE(cy_i_drop , NULL)
        PHP_FE(cy_i_next , NULL)
        PHP_FE(cy_i_info , NULL)
        PHP_FE(cy_i_reset, NULL)

        PHP_FE(cy_i_set , NULL)
        PHP_FE(cy_i_get , NULL)
        PHP_FE(cy_i_del , NULL)
        PHP_FE(cy_i_inc , NULL)
        PHP_FE(cy_i_dec , NULL)

        PHP_FE(cy_s_init , NULL)
        PHP_FE(cy_s_drop , NULL)
        PHP_FE(cy_s_next , NULL)
        PHP_FE(cy_s_info , NULL)
        PHP_FE(cy_s_reset, NULL)

        PHP_FE(cy_s_set , NULL)
        PHP_FE(cy_s_get , NULL)
        PHP_FE(cy_s_del , NULL)

        PHP_FE(cy_create_lock, NULL)
        PHP_FE(cy_lock, NULL)
        PHP_FE(cy_unlock, NULL)

        PHP_FE(cy_errno   , NULL)
        PHP_FE(cy_title   , NULL)

        PHP_FE(cy_ctl_check, NULL)
        PHP_FE(cy_ctl_succ , NULL)
        PHP_FE(cy_ctl_fail , NULL)

        PHP_FE(cy_split_by_tag, NULL)
        PHP_FE(cy_curl_multi_select, NULL)

        PHP_FE(cy_unpack   , NULL)
        PHP_FE(cy_pack     , NULL)

        PHP_FE_END      /* Must be the last line in cye_functions[] */
};
/* }}} */

/* {{{ cye_module_entry
 */
zend_module_entry cye_module_entry = {
#if ZEND_MODULE_API_NO >= 20010901
        STANDARD_MODULE_HEADER,
#endif
        "cye",
        cye_functions,
        PHP_MINIT(cye),
        PHP_MSHUTDOWN(cye),
        NULL,
        NULL,
        PHP_MINFO(cye),
#if ZEND_MODULE_API_NO >= 20010901
        "0.1", /* Replace with version number for your extension */
#endif
        STANDARD_MODULE_PROPERTIES
};
/* }}} */

BEGIN_EXTERN_C()
#ifdef COMPILE_DL_CYE
ZEND_GET_MODULE(cye)
#endif
END_EXTERN_C()

/* {{{ PHP_INI
 */
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("cye.shm_key"  ,  CY_SHMKEY       , PHP_INI_ALL, OnUpdateLong, shm_key  , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.shm_size" ,  CY_SHMSIZE      , PHP_INI_ALL, OnUpdateLong, shm_size , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.deep"     ,  CY_ROW_NUM      , PHP_INI_ALL, OnUpdateLong, deep     , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.cycle"    ,  CY_MAX_CTL_TIME , PHP_INI_ALL, OnUpdateLong, cycle    , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.max_fail" ,  CY_MAX_CTL_NUM  , PHP_INI_ALL, OnUpdateLong, max_fail , zend_cye_globals, cye_globals)
    STD_PHP_INI_ENTRY("cye.enable"   ,  "1"             , PHP_INI_ALL, OnUpdateLong, enable   , zend_cye_globals, cye_globals)
PHP_INI_END()
/* }}} */

/* {{{ php_cye_init_globals
 */
static void php_cye_init_globals(zend_cye_globals *cye_globals)
{

}
/* }}} */

/* {{{ PHP_MINIT_FUNCTION
*/
PHP_MINIT_FUNCTION(cye)
{
        REGISTER_INI_ENTRIES();

        cy_i_init(CY_HS_I_TYPE_CTL);
        cy_i_init(CY_HS_I_TYPE_SYS);
        cy_i_init(CY_HS_I_TYPE_CFG);

        cy_b_init(CY_HS_B_TYPE_CFG );
        cy_b_init(CY_HS_B_TYPE_LOCK);

        REGISTER_LONG_CONSTANT("CY_VERSION"         , 10000            , CONST_CS | CONST_PERSISTENT);
        REGISTER_LONG_CONSTANT("CY_HS_TYPE_SYS"     , CY_HS_I_TYPE_SYS , CONST_CS | CONST_PERSISTENT);
        REGISTER_LONG_CONSTANT("CY_HS_TYPE_CTL"     , CY_HS_I_TYPE_CTL , CONST_CS | CONST_PERSISTENT);
        REGISTER_LONG_CONSTANT("CY_HS_TYPE_CFG_I"   , CY_HS_I_TYPE_CFG , CONST_CS | CONST_PERSISTENT);

        REGISTER_LONG_CONSTANT("CY_HS_TYPE_LOCK"    , CY_HS_B_TYPE_LOCK, CONST_CS | CONST_PERSISTENT);
        REGISTER_LONG_CONSTANT("CY_HS_TYPE_CFG_S"   , CY_HS_B_TYPE_CFG , CONST_CS | CONST_PERSISTENT);

        REGISTER_LONG_CONSTANT("CY_TYPE_UINT8"      , CY_TYPE_UINT8     , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_UINT16"     , CY_TYPE_UINT16    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_UINT32"     , CY_TYPE_UINT32    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_UINT64"     , CY_TYPE_UINT64    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_STRING"     , CY_TYPE_STRING    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_ARRAY"      , CY_TYPE_ARRAY     , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_OBJECT"     , CY_TYPE_OBJECT    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_OBJKEY"     , CY_TYPE_OBJKEY    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_COND8"      , CY_TYPE_COND8     , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_COND16"     , CY_TYPE_COND16    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_COND32"     , CY_TYPE_COND32    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_COND64"     , CY_TYPE_COND64    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_TLV"        , CY_TYPE_TLV       , CONST_CS | CONST_PERSISTENT); 

        REGISTER_LONG_CONSTANT("CY_TYPE_CASE8"      , CY_TYPE_CASE8     , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_CASE16"     , CY_TYPE_CASE16    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_CASE32"     , CY_TYPE_CASE32    , CONST_CS | CONST_PERSISTENT); 
        REGISTER_LONG_CONSTANT("CY_TYPE_CASE64"     , CY_TYPE_CASE64    , CONST_CS | CONST_PERSISTENT); 

        REGISTER_LONG_CONSTANT("CY_COMMON_COND"     , CY_COMMON_COND    , CONST_CS | CONST_PERSISTENT); 


#ifndef PHP_SYSTEM_PROVIDES_SETPROCTITLE
        sapi_module_struct *symbol;
        symbol = &sapi_module;
        if(symbol)
        {
                _mc_G(proc_title) = symbol->executable_location;
        }
#endif

        return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
*/
PHP_MSHUTDOWN_FUNCTION(cye)
{
        UNREGISTER_INI_ENTRIES();
        return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
*/
PHP_MINFO_FUNCTION(cye)
{
        php_info_print_table_start();
        php_info_print_table_header(2, "cye support", "enabled");
        php_info_print_table_header(2, "By", "xiaoyjy@gmail.com");
        php_info_print_table_end();

        DISPLAY_INI_ENTRIES();
}
/* }}} */

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
