#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"

#if HAVE_CURL

#include "php_cye.h"
#include "cy_curl.h"

#include <curl/curl.h>
#include <curl/multi.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

/* {{{ proto int curl_multi_select(resource mh[, double timeout])
   Get all the sockets associated with the cURL extension, which can then be "selected" */
PHP_FUNCTION(cy_curl_multi_select)
{
        zval           *z_mh;
        php_curlm      *mh;
        fd_set          readfds, *p_fdr;
        fd_set          writefds, *p_fdw;
        fd_set          exceptfds, *p_fde;
        int             maxfd;
	int		flag = 0x0F;
        double          timeout = 1.0;
        struct timeval  to;
        unsigned long conv;

        if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "r|dl", &z_mh, &timeout, &flag) == FAILURE)
	{
                return;
        }

        ZEND_FETCH_RESOURCE(mh, php_curlm *, &z_mh, -1, le_curl_multi_handle_name, le_curl_multi_handle);

	conv = (unsigned long) (timeout * 1000000.0);
	to.tv_sec  = conv / 1000000;
	to.tv_usec = conv % 1000000;

	FD_ZERO(&readfds);
	FD_ZERO(&writefds);
	FD_ZERO(&exceptfds);

	curl_multi_fdset(mh->multi, &readfds, &writefds, &exceptfds, &maxfd);
	if (maxfd == -1) {
		RETURN_LONG(-1);
	}

	p_fdr = (flag & 0x01) > 0 ? &readfds   : NULL;
	p_fdw = (flag & 0x02) > 0 ? &writefds  : NULL;
	p_fde = (flag & 0x04) > 0 ? &exceptfds : NULL;
	RETURN_LONG(select(maxfd + 1, p_fdr, p_fdw, p_fde, &to));
}
/* }}} */

#endif
