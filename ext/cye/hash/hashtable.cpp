#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"

#include "php_cye.h"

#include "hash/hashmap.cc"
#include "hash/hashtable.h"

typedef hashmap_t<hashstr_t, uint32_t> IntHash_t;
typedef hashmap_t<hashstr_t, lv> BinHash_t;

static IntHash_t _g_cy_int[3];
static BinHash_t _g_cy_bin[2];

void cy_i_init(int type)
{
        IntHash_t *p = _g_cy_int + type;
        p->init(_mc_G(shm_key) + type, _mc_G(deep), _mc_G(shm_size));
}

void cy_i_drop(int type)
{
        IntHash_t *p = _g_cy_int + type;
        p->drop();
}

uint32_t cy_i_set(const char* key, int keylen, int type, uint32_t val)
{
        IntHash_t *p = _g_cy_int + type;
        hashstr_t hstr(key, keylen);

        return p->set(hstr, val);
}

uint32_t cy_i_inc(const char* key, int keylen, int type,  uint32_t val)
{
        IntHash_t *p = _g_cy_int + type;
        hashstr_t hstr(key, keylen);
        return p->inc(hstr, val);
}

uint32_t cy_i_dec(const char* key, int keylen, int type,  uint32_t val)
{
        IntHash_t *p = _g_cy_int + type;
        hashstr_t hstr(key, keylen);
        return p->dec(hstr, val);
}

uint32_t cy_i_get(const char* key, int keylen, int type)
{
        IntHash_t *p = _g_cy_int + type;
        hashstr_t hstr(key, keylen);

        uint32_t val;
        if(p->get(hstr, &val))
        {
                return val;
        }

        return 0;
}

uint32_t cy_i_del(const char* key, int keylen, int type)
{
        IntHash_t *p = _g_cy_int + type;
        hashstr_t hstr(key, keylen);
        return p->del(hstr);
}

uint32_t cy_i_next(char *key, int type, uint32_t *val)
{
        IntHash_t *p = _g_cy_int + type;
        hashstr_t hstr;
        int r = p->next(&hstr, val);
        if(r)
        {
                memcpy(key, &hstr.data, MAP_HASH_STR_LEN);
                return 1;
        }

        return 0;
}

uint32_t cy_i_reset(int type)
{
        IntHash_t *p = _g_cy_int + type;
        return p->reset();
}

void cy_b_init(int type)
{
        BinHash_t *p = _g_cy_bin + type;
        p->init(_mc_G(shm_key) + 5000 + type, _mc_G(deep), _mc_G(shm_size));
}

void cy_b_drop(int type)
{
        BinHash_t *p = _g_cy_bin + type;
        p->drop();
}

uint32_t cy_b_set(const char* key, int keylen, int type, lv *val)
{
        BinHash_t *p = _g_cy_bin + type;
        hashstr_t hstr(key, keylen);
        return p->set(hstr, *val);
}

lv* cy_b_get(const char* key, int keylen, int type)
{
        BinHash_t *p = _g_cy_bin + type;
        hashstr_t hstr(key, keylen);
        return p->get_ptr(hstr);
}

uint32_t cy_b_next(char *key, int type, lv *val)
{
        BinHash_t *p = _g_cy_bin + type;
        hashstr_t hstr;
        if(p->next(&hstr, val))
        {
                memcpy(key, &hstr.data, MAP_HASH_STR_LEN);
                return 1;
        }

        return 0;
}

uint32_t cy_b_del(const char* key, int keylen, int type)
{
        BinHash_t *p = _g_cy_bin + type;
        hashstr_t hstr(key, keylen);
        return p->del(hstr);
}

uint32_t cy_b_reset(int type)
{
        BinHash_t *p = _g_cy_bin + type;
        return p->reset();
}

/**
 * 如果失败，干三件事情
 *
 * 1、检查周期确认，已过周期时，清除临时错误计数器
 * 2、临时错误计数器+1
 * 3、永久错误计数器+1
 * 
 * key:f 临时错误计数据
 * key:t 计时周期的开始时间
 * key:pf 永久错误计数器
 */
uint32_t cy_ctl_fail(const char* key, int keylen, uint32_t now)
{
        char key_buf[MAP_HASH_STR_LEN];
        memcpy(key_buf, key, keylen);

        memcpy(key_buf + keylen, ":t", 2);
        hashstr_t h_time(key_buf, keylen + 2);

        memcpy(key_buf + keylen, ":f", 2);
        hashstr_t h_fail(key_buf, keylen + 2);

        memcpy(key_buf + keylen, ":pf", 3);
        hashstr_t h_pf(key_buf, keylen + 3);

        IntHash_t *p = _g_cy_int;
        p->inc(h_pf, 1);

        uint32_t time;
        if(!p->get(h_time, &time))
        {
                time = now;
                if(!p->set(h_time, now))
                {
                        return 0;
                }
        }

        if(now - time > _mc_G(cycle))
        {
                return p->set(h_fail, 1) & p->set(h_time, now);
        }
        else
        {
                return p->inc(h_fail, 1);
        }
}

/**
 * 如果成功，干两件事
 * 
 * 1、成功计数器加1
 * 2、失败计数器清0
 */
uint32_t cy_ctl_succ(const char* key, int keylen, uint32_t now)
{
        char key_buf[MAP_HASH_STR_LEN];
        memcpy(key_buf, key, keylen);

        memcpy(key_buf + keylen, ":tc", 3);
        hashstr_t h_succ(key_buf, keylen + 3);

        memcpy(key_buf + keylen, ":f", 2);
        hashstr_t h_fail(key_buf, keylen + 2);

        IntHash_t *p = _g_cy_int;
        return p->inc(h_succ, 1) & p->set(h_fail, 0); 
}

/**
 * 检查服务是否应该被block掉
 *
 * 1、如果错误计数器未到达上限，返回 active(1)
 * 2、检查是否已过计时周期，如过期清除屏蔽标记并返回 active(1)
 * 3、如果临时屏蔽标记已设置，返回 block(0)
 * 4、如果临时屏蔽标记未被设置， 设置临时标记，并将屏蔽统计计数器加1，返回block(0)
 *
 * key:f 临时错误计数器
 * key:b 临时屏蔽标记
 * key:t 计时周期的开始时间
 * key:pb 永久屏蔽次数统计计数器
 */
uint32_t cy_ctl_check(const char* key, int keylen, uint32_t now)
{
        char key_buf[MAP_HASH_STR_LEN];
        memcpy(key_buf, key, keylen);

        memcpy(key_buf + keylen, ":f", 2);
        hashstr_t h_fail(key_buf, keylen + 2);

        uint32_t fails;
        IntHash_t *p = _g_cy_int;

        if(!p->get(h_fail, &fails))
        {
                return 1;
        }

        if(fails < _mc_G(max_fail))
        {
                return 1;
        }

        uint32_t is_block;
        memcpy(key_buf + keylen, ":b", 2);
        hashstr_t h_block(key_buf, keylen + 2);
        if(!p->get(h_block, &is_block))
        {
                is_block = 0;
        }

        uint32_t time;
        memcpy(key_buf + keylen, ":t", 2);
        hashstr_t h_time(key_buf, keylen + 2);
        if(p->get(h_time, &time) && now - time > _mc_G(cycle))
        {
                if(is_block)
                {
                        p->set(h_block, 0);
                }

                return 1;
        }

        if(!is_block)
        {
                /**
                 * 为了实现方便，一次计时周期内屏蔽计数器只加一次
                 * 统计服务总共被block多少次, 屏蔽计数器加1
                 */
                memcpy(key_buf + keylen, ":pb", 3);
                hashstr_t h_pb(key_buf, keylen + 3);
                p->inc(h_pb   , 1);

                /* 设置临时屏蔽标记 */
                p->set(h_block, 1);
        }

        return 0;
}

uint32_t cy_i_info(int type, char *info, int len)
{
        IntHash_t *p = _g_cy_int + type;
        p->info(info, len);
        return 1;
}

uint32_t cy_b_info(int type, char *info, int len)
{
        BinHash_t *p = _g_cy_bin + type;
        p->info(info, len);
        return 1;
}

