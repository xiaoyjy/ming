dnl $Id$
dnl config.m4 for extension cye

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

AC_DEFINE(CYE_DEBUG,0,[ if is debug version ])

dnl If your extension references something external, use with:

dnl Make sure that the comment is aligned:
PHP_ARG_WITH(cye, for cye support,
[  --with-cye             Include cye support])

dnl Otherwise use enable:

dnl Make sure that the comment is aligned:
PHP_ARG_ENABLE(cye, whether to enable cye support,
[  --enable-cye           Enable cye support])

if test "$PHP_CYE" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-cye -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/cye.h"  # you most likely want to change this
  dnl if test -r $PHP_CYE/$SEARCH_FOR; then # path given as parameter
  dnl   CYE_DIR=$PHP_CYE
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for cye files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       CYE_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$CYE_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the cye distribution])
  dnl fi

  dnl # --with-cye -> add include path
  dnl PHP_ADD_INCLUDE($CYE_DIR/include)
  PHP_CURL=/home/xiaoyjy/ming-sys
  if test -r $PHP_CURL/include/curl/easy.h; then
    CURL_DIR=$PHP_CURL
  else
    AC_MSG_CHECKING(for cURL in default path)
    for i in /usr/local /usr; do
      if test -r $i/include/curl/easy.h; then
        CURL_DIR=$i
        AC_MSG_RESULT(found in $i)
        break
      fi
    done
  fi

  if test -z "$CURL_DIR"; then
    AC_MSG_RESULT(not found)
    AC_MSG_ERROR(Please reinstall the libcurl distribution -
    easy.h should be in <curl-dir>/include/curl/)
  fi

  CURL_CONFIG="curl-config"
  AC_MSG_CHECKING(for cURL 7.10.5 or greater)

  if ${CURL_DIR}/bin/curl-config --libs > /dev/null 2>&1; then
    CURL_CONFIG=${CURL_DIR}/bin/curl-config
  else
    if ${CURL_DIR}/curl-config --libs > /dev/null 2>&1; then
      CURL_CONFIG=${CURL_DIR}/curl-config
    fi
  fi

  curl_version_full=`$CURL_CONFIG --version`
  curl_version=`echo ${curl_version_full} | sed -e 's/libcurl //' | $AWK 'BEGIN { FS = "."; } { printf "%d", ($1 * 1000 + $2) * 1000 + $3;}'`
  if test "$curl_version" -ge 7010005; then
    AC_MSG_RESULT($curl_version_full)
    CURL_LIBS=`$CURL_CONFIG --libs`
  else
    AC_MSG_ERROR(cURL version 7.10.5 or later is required to compile php with cURL support)
  fi

  PHP_ADD_INCLUDE($CURL_DIR/include)
  PHP_EVAL_LIBLINE($CURL_LIBS, CYE_SHARED_LIBADD)
  PHP_ADD_LIBRARY_WITH_PATH(curl, $CURL_DIR/$PHP_LIBDIR, CYE_SHARED_LIBADD)    

  dnl # --with-cye -> check for lib and symbol presence
  dnl LIBNAME=cye # you may want to change this
  dnl LIBSYMBOL=cye # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $CYE_DIR/lib, CYE_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_CYELIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong cye lib version or lib not found])
  dnl ],[
  dnl   -L$CYE_DIR/lib -lm
  dnl ])
  dnl
  dnl PHP_SUBST(CYE_SHARED_LIBADD)
  AC_CACHE_VAL(ac_cv_php_system_provides_setproctitle_call,[
  AC_CHECK_FUNCS(setproctitle, [
          php_system_provides_setproctitle_call=yes
          break
      ],[
          php_system_provides_setproctitle_call=no
  ])])

  AC_MSG_CHECKING([if your OS provides a native way to change a process title])
  if test "$php_system_provides_setproctitle_call" = "yes"; then
      AC_MSG_RESULT(yes)
      AC_DEFINE(PHP_SYSTEM_PROVIDES_SETPROCTITLE, 1, [Define if your system has setproctitle])
  else
      AC_MSG_RESULT(no)
  fi

  PHP_ADD_INCLUDE(hash)
  PHP_ADD_INCLUDE(include)

  PHP_SUBST(CYE_SHARED_LIBADD)

  PHP_REQUIRE_CXX() 
  PHP_ADD_LIBRARY(stdc++, "", EXTRA_LDFLAGS)
  PHP_NEW_EXTENSION(cye, cy_ctl.c cy_string.c cy_pack.c cy_curl.c cy_fe.c hash/hashtable.cpp lib/calculator.cpp lib/str.c, $ext_shared)
fi
