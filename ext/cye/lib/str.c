#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <byteswap.h>
#include <arpa/inet.h>

void cy_w_int8 (char **ptr, uint16_t v)
{
	*(*ptr)++ = v;
}

void cy_w_int16(char **ptr, uint16_t v)
{
	uint16_t sv;
	sv = htons(v);
	memcpy(*ptr, &sv, 2);
	*ptr += 2;
}

void cy_w_int32(char **ptr, uint32_t v)
{
	uint32_t iv;
	iv = htonl(v);
	memcpy(*ptr, &iv, 4);
	*ptr += 4;
}

void cy_w_int64(char **ptr, uint64_t v)
{
	uint64_t lv;
#if __BYTE_ORDER == __LITTLE_ENDIAN
	lv = bswap_64(v);
#endif
	memcpy(*ptr, &lv, 8);
	*ptr += 8;
}

unsigned long cy_r_int8 (const char **ptr)
{
	return (unsigned char)*(*ptr)++;
}

unsigned long cy_r_int16(const char **ptr)
{
	uint16_t sv;
	memcpy(&sv, *ptr, 2);
	*ptr += 2;

	return (unsigned short)ntohs(sv);
}

unsigned long cy_r_int32(const char **ptr)
{
	uint32_t iv;
	memcpy(&iv, *ptr, 4);
	*ptr += 4;

	return (unsigned int)ntohl(iv);
}

unsigned long cy_r_int64(const char **ptr)
{
	uint64_t lv;
	memcpy(&lv, *ptr, 8);
	*ptr += 8;

#if defined __x86_64__

#if __BYTE_ORDER == __LITTLE_ENDIAN
	return (unsigned long)bswap_64(lv);
#else
	return (unsigned long)lv;
#endif

#else
	return 0;
#endif

}

void cy_dump_binary(const void* data, size_t len)
{
	const unsigned char *p = (const unsigned char*)data;
	int i;
	for(i = 0; i < len; i++)
	{
		if(i%2 == 0 && i != 0)
		{
			php_printf(" ");
		}

		if(i%16 == 0)
		{
			if(i == 0)
			{
				php_printf("-----------------------------------------------\n");
				php_printf("  |\t0001 0203 0405 0607 0809 0A0B 0C0D 0E0F\n");
				php_printf("-----------------------------------------------\n00|\t");
			}
			else
			{
				php_printf("\n%02X|\t", (i/16));
			}
		}

		php_printf("%02x", p[i]);
	}

	php_printf("\n\n");
}

const char* cy_bin2str(const unsigned char* data, size_t len, char* str)
{
	char *p = str;
	int i, n;
	for(i = 0; i < len; i++)
	{
		n  = sprintf(p, "%02x", data[i]);
		p += n;
		//
		//if(i%16) *p++ = ' ';
	}

	*p = '\0';
	return str;
}

const char* cy_str2bin(const unsigned char* str, size_t len, char* data)
{
	int i;
	const unsigned char *p = str;
	len = len/2;
	for(i = 0; i < len; ++i)
	{
		sscanf(p, "%02x", (unsigned int*)(data + i));
		p += 2;
	}

	return data;   
}

const char* cy_bin2str_ex(const unsigned char* data, size_t len, char* str)
{
	char *p = str;
	int i;
	for(i = 0; i < len; i++)
	{
		sprintf(p, "%02x", data[i]);
		p += 2;

		if(i%2  == 1 ) *p++ = ' ';
		if(i%16 == 15) *p++ = '\n';
	}

	*p = '\0';
	return str;
}

