#ifndef __CY_CALCULATOR_H__
#define __CY_CALCULATOR_H__

#include <sys/types.h>

int cy_calculator(const char* str, size_t length);

#endif
