#ifndef __CY_PACK_H_
#define __CY_PACK_H_

#include <php.h>

extern zval* unpack_output(const char* output, size_t size, zval *config, zval *dest, size_t *used, zval *ctl_root);
extern const char* pack_input(zval *inputs, char* inbuf, size_t* inlen);

#endif

