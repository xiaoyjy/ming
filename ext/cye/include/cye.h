#ifndef __CY_DEFINE_H_
#define __CY_DEFINE_H_

#include <stdint.h>

#define CY_BUFFER_LENGTH 8192

typedef enum _CY_Config_Type
{
	CY_TYPE_UINT8  = 0 ,
	CY_TYPE_UINT16  ,
	CY_TYPE_UINT32  ,
	CY_TYPE_UINT64  ,

	CY_TYPE_STRING  ,
	CY_TYPE_ARRAY   ,
	CY_TYPE_OBJECT  ,

	CY_TYPE_OBJKEY  ,

	CY_TYPE_COND8   ,
	CY_TYPE_COND16  ,
	CY_TYPE_COND32  ,
	CY_TYPE_COND64  ,

	CY_TYPE_TLV     ,

	CY_TYPE_CASE8   ,
	CY_TYPE_CASE16  ,
	CY_TYPE_CASE32  ,
	CY_TYPE_CASE64  ,
}
CY_Config_Type;

#define CY_COMMON_COND 0x7FFFFFFF

#ifndef BEGIN_EXTERN_C

  #ifdef __cplusplus
    #define BEGIN_EXTERN_C() extern "C" {
    #define END_EXTERN_C() }
  #else
    #define BEGIN_EXTERN_C()
    #define END_EXTERN_C()
  #endif

#endif

#endif
