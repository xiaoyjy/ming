--TEST--
Curl recv

continuous append request and recv response.

--FILE--
<?php

include dirname(dirname(__DIR__))."/src/init.php";

$cl = new CY_Util_Curl();

$headers  = ['User-Agent: Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'];
$options  = ['timeout' => 4000, 'include' => true];

$urls = new SplQueue();
$urls[] = 'http://www.qq.com';
$urls[] = 'http://www.notexists-null-null.com';
$urls[] = 'http://www.baidu.com';
$urls[] = 'http://www.sina.com.cn';
$urls[] = 'http://www.163.com';


$data = [];

$i = 0;
while(!$urls->isEmpty() && ($url = $urls->pop()))
{
	$c = new CY_Driver_Http_Default($url, 'GET', $headers, $options);
	$cl->add($i++, $c);

	do
	{
		$dt = $cl->recv();
		if($cl->size() > 10)
		{
			usleep(50000);
		}

		foreach($dt['data'] as $r)
		{
			$data[] = $r['errno']."\t".$r['url'];
		}
		
	}
	while($cl->size() > 2);
}


do
{
	$dt = $cl->recv();
	if($cl->size() > 10)
	{
		usleep(50000);
	}

	foreach($dt['data'] as $r)
	{
		$data[] = $r['errno']."\t".$r['url'];
	}

}
while($cl->size() > 0);



sort($data); 




sort($data); 

print_r($data);

?>
--EXPECT--
Array
(
    [0] => 0	http://www.163.com
    [1] => 0	http://www.baidu.com
    [2] => 0	http://www.notexists-null-null.com
    [3] => 0	http://www.qq.com
    [4] => 0	http://www.sina.com.cn
)
