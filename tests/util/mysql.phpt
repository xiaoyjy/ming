--TEST--
CY_Util_MySQL

tests INSERT/UPDATE/DELETE/SELECT/CREATE/DROP.

--FILE--
<?php
include dirname(dirname(__DIR__)).'/src/init.php';

$db = new CY_Util_Mysql();

$string_drop   = <<<DROP
DROP TABLE IF EXISTS test_create_table_001 ;
DROP;

echo "\n---TEST DROP TABLE---\n";
$dt = $db->query($string_drop);
echo $dt['errno'] === 0 ? 'OK' : "Failed";

$string_create = <<<CREATE
CREATE TABLE test_create_table_001 (
     id MEDIUMINT NOT NULL AUTO_INCREMENT,
     data CHAR(255) NOT NULL,
     PRIMARY KEY (id)
)
CREATE;

echo "\n---TEST CREATE TABLE---\n";
$dt = $db->query($string_create);
echo $dt['errno'] === 0 ? 'OK' : "Failed";

$string_insert_sync = <<<INSERT
INSERT INTO `test_create_table_001` SET `data`='vvvvvvvvvvvv'
INSERT;

echo "\n---TEST INSERT sync ---\n";
$dt = $db->query($string_insert_sync);
echo $dt['errno'] === 0 ? 'OK' : "Failed";

echo "\n---TEST INSERT Async ---\n";
$request1 = ['data' => [['data' => '1'], ['data' => 2]]];
$request2 = ['data' => [['data' => '3'], ['data' => 4]]];
$c1 = new CY_Driver_DB_Default('test_create_table_001', $request1, ['method' => 'insert']); 
$c2 = new CY_Driver_DB_Default('test_create_table_001', $request2, ['method' => 'insert']); 
$db->add(1, $c1);
$db->add(2, $c2);
$dt = $db->mGet();
print_r(($dt));

echo "\n---TEST update Async ---\n";
$request1 = ['data' => ['data' => 'test1'], 'where' => ['id' => [1]]];
$request2 = ['data' => ['data' => 'hello test2!'], 'where' => ['id' => [2]]];
$c1 = new CY_Driver_DB_Default('test_create_table_001', $request1, ['method' => 'update']); 
$c2 = new CY_Driver_DB_Default('test_create_table_001', $request2, ['method' => 'update']); 
$db->add(3, $c1);
$db->add(4, $c2);
$dt = $db->mGet();
echo $dt['errno'] === 0 ? 'OK' : "Failed";

echo "\n---TEST select Async ---\n";
$request1 = ['where' => ['id' => [1, 4]]];
$request2 = ['where' => ['id' => [2, 3]]];
$c1 = new CY_Driver_DB_Default('test_create_table_001', $request1); 
$c2 = new CY_Driver_DB_Default('test_create_table_001', $request2); 
$db->add(3, $c1);
$db->add(4, $c2);
$dt = $db->mGet();
print_r(cy_dt_m($dt));


$dt = $db->query($string_drop);

?>
--EXPECT--

---TEST DROP TABLE---
OK
---TEST CREATE TABLE---
OK
---TEST INSERT sync ---
OK
---TEST INSERT Async ---
Array
(
    [errno] => 0
    [data] => Array
        (
            [1] => Array
                (
                    [errno] => 0
                    [data] => Array
                        (
                            [affected_rows] => 2
                            [insert_id] => 2
                        )

                )

            [2] => Array
                (
                    [errno] => 0
                    [data] => Array
                        (
                            [affected_rows] => 2
                            [insert_id] => 4
                        )

                )

        )

)

---TEST update Async ---
OK
---TEST select Async ---
Array
(
    [errno] => 0
    [data] => Array
        (
            [1] => Array
                (
                    [id] => 1
                    [data] => test1
                )

            [4] => Array
                (
                    [id] => 4
                    [data] => 3
                )

            [2] => Array
                (
                    [id] => 2
                    [data] => hello test2!
                )

            [3] => Array
                (
                    [id] => 3
                    [data] => 2
                )

        )

)
