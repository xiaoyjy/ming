--TEST--
Extrcator

extract title.

--FILE--
<?php

include dirname(dirname(__DIR__))."/src/init.php";

if(isset($argv[1]))
{
	echo $argv[1];
	test_parse(file_get_contents($argv[1]));
	exit;
}

foreach(glob(dirname(__DIR__).'/data/exctractor/*') as $filename)
{
	echo basename($filename);
	test_parse(file_get_contents($filename));
}

function test_parse($content)
{
	$charset = cy_html_charset($content);
	if($charset !== 'UTF-8' && $charset !== 'ISO-8859-1')
	{
		$content = iconv('GBK', 'UTF-8', $content);
	}

	$ex = new CA_Util_Extractor();
	$ex->load($content);
	echo "\t", $ex->title(), "\n";
//print_r($ex->links('www.test.com', '/path'));
}

?>
--EXPECT--
011346.htm	吴稚晖：把中国的线装书扔到茅厕里
1083096	【西安】热门港式茶餐厅 TVB迷心头最爱
123	舌尖上的美味跳动的羊肉8.8折
156880	酸豆角米粉
173628279295.shtml	习近平：向四川因公殉职副县长兰辉学习
1827702	点石成金
21216	一个懂用户体验的网络产品经理必备的几本书。
biz-228601	天华毛家菜馆(天宁寺店)
c-40-744-1.html	马自达CX-5强势登场 博瑞祥浓4s店现车销售
index.html	Don&#39;t Make Me Think
news_1251830_10.html	自动顶级版30城行情汇总
