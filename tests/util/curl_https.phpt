--TEST--
Curl https

https tests.
--FILE--
<?php

include dirname(dirname(__DIR__))."/src/init.php";

$cl = new CY_Util_Curl();

$headers  = ['User-Agent: Mozilla/5.0 (compatible; APPI'];
$options  = ['timeout' => 8000, 'include' => true];

$url = 'https://bitbucket.org';
$c   = new CY_Driver_Http_Default($url, 'GET', $headers, $options);
$cl->add('test', $c);

$r = $cl->get();

print_r($r);

?>


