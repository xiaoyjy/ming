--TEST--
Extract html 

time, title, content

--FILE--
<?php

include dirname(dirname(__DIR__))."/src/init.php";

$t1 = microtime(true);

$filename = dirname(__DIR__).'/data/exctract/014144FFA5D820D0FF8080814141CFC1';
$contents = file_get_contents($filename);

$url = 'http://www.yododo.com/area/guide/014144FFA5D820D0FF8080814141CFC1';

$ex = new CA_Util_Extractor();
$ex->load($contents, ['url' => $url]);

echo $ex->time(), "\n";
$title = $ex->title();

echo $title, "\n";
$content = $ex->content(['title' => $title]);
echo strpos($content, '东盟媒体考察团旅行”圆满结束') !== false;
echo "\n";


?>
--EXPECT--
2013-09-22 17:28
2013年9月6日~11日 神奇泰国——东盟媒体考察团旅行
1
