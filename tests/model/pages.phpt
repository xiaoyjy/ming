--TEST--
CH_Model_Pages

This test need network up.

--FILE--
<?php

include dirname(dirname(__DIR__))."/src/init.php";

$pages = new CH_Model_Pages();

$content = file_get_contents('http://www.qq.com');
//$content = gzcompress($content, 9);
$content = iconv("GBK", 'UTF-8', $content);
$data = ['url' => 'http://www.qq.com', 'md5' => md5('http://www.qq.com'), 'content' => $content];
$dt = $pages->mSet([$data]);
echo count($dt['data']);
echo "\n";

$id = $dt['data'][0];
$dt = $pages->delete(['id' => [$id]]);
var_dump($dt);

?>
--EXPECT--
1
array(2) {
  ["errno"]=>
  int(0)
  ["data"]=>
  bool(true)
}
