--TEST--
CY_Model_Place::mGet

get data form place table.

--FILE--
<?php
include dirname(dirname(__DIR__)).'/init.php';

$md = new CY_Model_Place();
$rq = array('id' => [7], 'title' => ['故宫']);
$dt = $md->mGet($rq);

foreach($dt['data'] as $val)
{
	echo $val['title']."\n";
}
?>
--EXPECT--
豆香园
故宫
